<?php
/**
 * Generic Shop Payments Form
 *
 * The file is for displaying the Generic Shop payment form
 * Copyright (c) Generic Shop
 *
 * @package     Genericshop/Templates
 * @located at  /template/ckeckout/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<script type="text/javascript">
	var wpwlOptions = {
		locale: "<?php echo esc_attr( strtolower( substr( get_bloginfo( 'language' ), 0, 2 ) ) ) ?>",
		style: "card",
		onReady: function() {
			var buttonCancel = "<a href='<?php echo esc_attr( $url_config['cancel_url'] ) ?>' class='wpwl-button btn_cancel'><?php echo esc_attr( __( 'FRONTEND_BT_CANCEL', 'wc-genericshop' ) ) ?></a>";
			var ttTestMode = "<div class='testmode'><?php echo esc_attr( __( 'FRONTEND_TT_TESTMODE', 'wc-genericshop' ) ) ?></div>";
			var merchantLocation = "<div class='merchant-location-description'><?php echo esc_attr( __( 'FRONTEND_MERCHANT_LOCATION_DESC', 'wc-genericshop' ) ) . esc_attr( $merchant_location ); ?></div>";
			jQuery( "form.wpwl-form" ).find( ".wpwl-button" ).before( buttonCancel );
			<?php if ( $merchant_location && ('genericshop_cc' === $payment_method || 'genericshop_ccsaved' === $payment_method) ) : ?>
				jQuery( ".wpwl-container" ).after( merchantLocation );
			<?php endif; ?>
			<?php if ( 'TEST' === $settings['server_mode'] ) : ?>
				jQuery( ".wpwl-container" ).wrap( "<div class='frametest'></div>" );
				jQuery( ".wpwl-container" ).before( ttTestMode );
			<?php endif; ?>
			<?php if ( $is_recurring && $is_one_click_payments ) : ?>
				var headerWidget = "<h3 id='deliveryHeader' style='text-align:center'><?php echo esc_attr( __( 'FRONTEND_RECURRING_WIDGET_HEADER2', 'wc-genericshop' ) ) ?></h3>";
				jQuery( "#wpwl-registrations" ).after( headerWidget );
				if (jQuery('.merchant-location-description').length > 1){
		            jQuery('.merchant-location-description').eq(0).hide();
		        }
			<?php endif; ?>
		},
		registrations: {
			hideInitialPaymentForms: false,
			requireCvv: false
		}
	}
</script>
<?php if ( $is_recurring ) : ?>
	<?php if ( $is_one_click_payments ) : ?>
		<h3 id="deliveryHeader" style="text-align:center"><?php echo esc_attr( __( 'FRONTEND_RECURRING_WIDGET_HEADER1', 'wc-genericshop' ) ) ?></h3>
	<?php else : ?>
		<h3 id="deliveryHeader" style="text-align:center"><?php echo esc_attr( __( 'FRONTEND_MC_PAYANDSAFE', 'wc-genericshop' ) ) ?></h3>
	<?php endif; ?>
<?php endif; ?>

<form action="<?php echo esc_attr( $url_config['return_url'] ) ?>" class="paymentWidgets"><?php echo esc_attr( $payment_parameters['payment_brand'] ) ?></form>
