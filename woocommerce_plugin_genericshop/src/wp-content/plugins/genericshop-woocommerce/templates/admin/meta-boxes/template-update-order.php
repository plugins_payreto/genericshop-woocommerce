<?php
/**
 * Generic Shop Payments Update Order
 *
 * The file is for displaying button update order at order detail ( admin )
 * Copyright (c) Generic Shop
 *
 * @package     Genericshop/Templates
 * @located at  /template/admin/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<p class="form-field form-field-wide" style="text-align:right">
	<label for="order_status">&nbsp;</label>
	<a href="<?php echo esc_attr( get_admin_url() ) ?>post.php?post=<?php echo esc_attr( $order_id ); ?>&action=edit&section=update-order" class="button save_order button-primary" >Update Order</a>
</p>
