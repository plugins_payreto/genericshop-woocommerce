<?php
/**
 * Generic Shop Registered Payment ( AJAX )
 *
 * The file is for add registered payment method for backend order
 * Copyright (c) Generic Shop
 *
 * @package     Genericshop/Templates
 * @located at  /template/admin/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
if ( 'edit' === $action ) :
?>
	<script type="text/javascript" >
		jQuery( document ).ready( function( $ ) {
			
			$( "#_payment_method" ).parent().hide();

		} );
	</script>

<?php
else :
?>

	<div style="clear:both"></div>
	<div class="edit_address">

		<p class="form-field _transaction_id_field" id="generic_payment">
			<label for="payment_recurring">Payment Recurring:</label>
			<select name="_payment_recurring" id="_payment_recurring" class="first"></select>

		</p>
	</div>
	<div style="clear:both"></div>

	<script type="text/javascript" >

	jQuery( document ).ready( function( $ ) {
		getGenericPaymentMethod( $ );

		$( "#_payment_method" ).click( function() {
		getGenericPaymentMethod( $ );
		} );

		$( "#_payment_method" ).change( function() {
			getGenericPaymentMethod( $ );
		} );

		$( "#_payment_method" ).keyup( function() {
			getGenericPaymentMethod( $ );
		} );	
	} );

	function getGenericPaymentMethod( $ ) {

		$( "#generic_payment" ).hide();
		var payment_id = $( "#_payment_method" ).val();
		var user_id = $( '#customer_user' ).val();

		var data = {
			'action': 'my_action',
			'user_id': user_id,
			'payment_id': payment_id,
		};

		jQuery.post( ajaxurl, data, function( response ) {
		
			var registered_payments = $.parseJSON( response );

			if ( registered_payments.length === 0 ) {
				$( '#_payment_recurring' ).html( $( '<option>', {
						value: 0,
						text : 'N/A'
				} ) );
			} else {
				if ( payment_id === 'genericshop_ccsaved' ) {
					$.each( registered_payments, function ( i, payment ) {
						if ( i === 0 ) {
							$( '#_payment_recurring' ).html( $( '<option>', {
								value: payment.reg_id,
								text : 'Credit Cards - '+ payment.brand + ' ( ending in: ' + payment.last4digits + ' ; expires on: ' + payment.expiry_year + ' )'
							} ) );
						}else{
							$( '#_payment_recurring' ).append( $( '<option>', {
								value: payment.reg_id,
								text : 'Credit Cards - '+ payment.brand + ' ( ending in: ' + payment.last4digits + ' ; expires on: ' + payment.expiry_year + ' )'
							} ) );
						}
					} );
				}
			}
			if ( payment_id === 'genericshop_ccsaved'
			 ) {
				$( "#generic_payment" ).show();
			}
		} );
	}
	</script>

<?php
endif;
