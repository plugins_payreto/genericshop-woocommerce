<?php
/**
 * Generic Shop Payments Update Order
 *
 * The file is for displaying additional information at order detail ( admin )
 * Copyright (c) Generic Shop
 *
 * @package     Genericshop/Templates
 * @located at  /template/admin/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<div class='additional_information' style='display: block; float: left'>
	<p>
		<strong style='display: block'><?php echo esc_attr( __( 'BACKEND_GENERAL_INFORMATION', 'wc-genericshop' ) ); ?></strong>
		<?php
		echo esc_attr( __( 'BACKEND_TT_PAYMENT_METHOD', 'wc-genericshop' ) ) . ' : ' . esc_attr( $payment_method_title ) . '<br />';
		echo esc_attr( __( 'BACKEND_TT_CURRENCY', 'wc-genericshop' ) ) . ' : ' . esc_attr( $transaction_log['currency'] ) . '<br />';
		echo esc_attr( __( 'BACKEND_TT_AMOUNT', 'wc-genericshop' ) ) . ' : ' . esc_attr( $transaction_log['amount'] ) . '<br />';
		echo esc_attr( __( 'BACKEND_TT_TRANSACTION_ID', 'wc-genericshop' ) ) . ' : ' . esc_attr( $transaction_log['transaction_id'] ) . '<br />';
		if ( $additional_information ) {
			foreach ( $additional_information as $info_name => $info_value ) {
				echo  esc_attr( Genericshop_General_Functions::grs_translate_additional_information( $info_name ) ) . ' : ' . esc_attr( $info_value ) . '<br />';
			}
		}
		?>
	</p>
</div>
