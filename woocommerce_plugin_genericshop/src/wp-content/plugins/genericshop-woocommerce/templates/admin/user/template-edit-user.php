<?php
/**
 * Generic Shop Payments Form
 *
 * The file is for add extra field at backend user
 * Copyright (c) Generic Shop
 *
 * @package     Genericshop/Templates
 * @located at  /template/admin/user/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<h3><?php echo esc_attr( $label['costumer'] ); ?></h3>
<table class="form-table">
<tr>
	<th><label for="dob"><?php echo esc_attr( $label['dob'] ); ?></label></th>
	<td>
		<input type="text" name="generic_billing_dob" id="generic_billing_dob" class="regular-text" value="<?php echo esc_attr( $dob ); ?>" />
		<br />
		<span class="description"><?php echo esc_attr( $label['dob_placeholder'] ); ?></span>
	</td>
</tr>
<tr>
	<th><label for="gender"><?php echo esc_attr( $label['gender'] ); ?></label></th>
	<td>
		<select id="generic_billing_gender" name="generic_billing_gender" title="Gender" style="width: 25em; padding: 1px;">
			<option value="Male" <?php if ( 'Male' === $gender || 'Männlich' === $gender ) { echo 'selected="selected"'; } ?>><?php echo esc_attr( $label['gender_male'] ); ?></option>
			<option value="Female" <?php if ( 'Female' === $gender || 'Weiblich' === $gender ) { echo 'selected="selected"'; } ?>><?php echo esc_attr( $label['gender_female'] ) ?></option>
		</select>
	</td>
</tr>
</table>
