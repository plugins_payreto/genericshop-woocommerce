<?php
/**
 * Generic Shop Payments Deregister Form
 *
 * The file is for displaying the Generic Shop deregister form
 * Copyright (c) Generic Shop
 *
 * @package     Genericshop/Templates
 * @located at  /template/ckeckout/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<h2 class="header-title"><?php echo esc_attr( __( 'FRONTEND_MC_DELETE', 'wc-genericshop' ) ) ?></h2>

<div class="box-unreg">
	<form action="<?php echo esc_attr( $url_config['return_url'] ) ?>" method="post">
		<p class="text-unreg"><?php echo esc_attr( __( 'FRONTEND_MC_DELETESURE', 'wc-genericshop' ) ) ?></p>
		<a class="btnCustom btnLink button-primary" href="<?php echo esc_attr( $url_config['cancel_url'] ) ?>"><?php echo esc_attr( __( 'FRONTEND_BT_CANCEL', 'wc-genericshop' ) ) ?></a>
		<button class="btnCustom button-primary" type="submit" value="submit"><?php echo esc_attr( __( 'FRONTEND_BT_CONFIRM', 'wc-genericshop' ) ) ?></button>
	</form>
</div>

