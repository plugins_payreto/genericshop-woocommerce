<?php
/**
 * Plugin Name: Generic Shop - WooCommerce
 * Plugin URI:  hhttp://www.generic-shop.info/
 * Description: WooCommerce with Generic Shop payment gateway
 * Author:      Generic Shop
 * Author URI:  http://www.generic-shop.info/
 * Version:     1.0.0
 *
 * @package     Genericshop/Classes
 */

/**
 * Copyright (c) Generic Shop
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

include_once( dirname( __FILE__ ) . '/genericshop-install.php' );
include_once( dirname( __FILE__ ) . '/genericshop-additional.php' );
register_activation_hook( __FILE__ , 'grs_activation_process' );
register_deactivation_hook( __FILE__ , 'grs_deactivation_process' );
register_uninstall_hook( __FILE__ , 'grs_uninstallation_process' );
add_action( 'plugins_loaded', 'init_payment_gateway', 0 );

ob_start();

define( 'GENERIC_VERSION', '1.0.0' );
define( 'GENERIC_PLUGIN_FILE', __FILE__ );

/**
 * Check if WooCommerce is active, and if it isn't, disable Generic Shop payments.
 */
function get_notice_woocommerce_activation() {

	echo '<div id="notice" class="error"><p>';
	echo '<a href="http://www.woothemes.com/woocommerce/" style="text-decoration:none" target="_new">WooCommerce </a>' . esc_attr( __( 'BACKEND_GENERAL_PLUGINREQ', 'wc-genericshop' ) ) . '<b> Generic Shop Payment Gateway for WooCommerce</b>';
	echo '</p></div>';
}

/**
 * Add configuration link at plugin installation
 *
 * @param array $links links.
 * @return array
 */
function add_configuration_links( $links ) {
	$add_links = array( '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=genericshop_settings' ) . '">' . __( 'BACKEND_CH_GENERAL','wc-genericshop' ) . '</a>' );
	return array_merge( $add_links , $links );
}
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'add_configuration_links' );

/**
 * Validate genericshop term
 */
function validate_genericshop_term() {
	if ( ! get_option( 'genericshop_term' ) ) {
		echo '<div id="notice" class="updated"><p>';
		echo esc_attr( Genericshop_General_Functions::grs_translate_genericshop_term( 'GENERICSHOP_TT_VERSIONTRACKER' ) );
		echo ' <a href="' . esc_attr( admin_url( 'admin.php?page=wc-settings&tab=genericshop_settings' ) ) . '">' . esc_attr( Genericshop_General_Functions::grs_translate_genericshop_term( 'GENERICSHOP_BACKEND_BT_ADMIN' ) ) . '</a>';
		echo '</p></div>';
		add_option( 'genericshop_term', true );
	}
}

/**
 * Init payment gateway
 */
function init_payment_gateway() {

	/**
	 * Loads the Genericshop language translation strings
	 */
	load_plugin_textdomain( 'wc-genericshop', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

	if ( ! class_exists( 'WC_Payment_Gateway' ) ) {
		add_action( 'admin_notices', 'get_notice_woocommerce_activation' );
		return;
	} else {
		add_action( 'admin_notices', 'validate_genericshop_term' );
	}

	include_once( dirname( __FILE__ ) . '/includes/class-genericshop-general-functions.php' );
	include_once( dirname( __FILE__ ) . '/includes/class-genericshop-general-models.php' );
	include_once( dirname( __FILE__ ) . '/includes/admin/class-genericshop-general-settings.php' );
	include_once( dirname( __FILE__ ) . '/includes/myaccount/class-genericshop-payment-information.php' );
	include_once( dirname( __FILE__ ) . '/includes/core/class-genericshoppaymentcore.php' );
	include_once( dirname( __FILE__ ) . '/includes/core/class-genericshopversiontracker.php' );

	if ( ! class_exists( 'Genericshop_Payment_Gateway' ) ) {

		/**
		 * Genericshop Payment Gateway
		 *
		 * @class Genericshop_Payment_Gateway
		 */
		class Genericshop_Payment_Gateway extends WC_Payment_Gateway {
			/**
			 * Payment id
			 *
			 * @var string $payment_id
			 */
			protected $payment_id;

			/**
			 * Payment type
			 *
			 * @var string $payment_type
			 */
			protected $payment_type = 'DB';

			/**
			 * Payment brand
			 *
			 * @var string $payment_brand
			 */
			protected $payment_brand;

			/**
			 * Payment group
			 *
			 * @var string $payment_group
			 */
			protected $payment_group;
			/**
			 * Is recurring payment or not
			 *
			 * @var bool $is_recurring
			 */
			protected $is_recurring = false;

			/**
			 * When doing payment in TEST system. use this value as testMode parameter.
			 *
			 * @var string $test_mode
			 */
			protected $test_mode = 'EXTERNAL';

			/**
			 * Payment display
			 *
			 * @var string $payment_display
			 */
			protected $payment_display = 'form';

			/**
			 * Languange
			 *
			 * @var string $language
			 */
			protected $language;

			/**
			 * Saved meta boxes
			 *
			 * @var bool $saved_meta_boxes
			 */
			private static $saved_meta_boxes = false;

			/**
			 * Added meta boxes
			 *
			 * @var bool $added_meta_boxes
			 */
			private static $added_meta_boxes = false;

			/**
			 * Added payment method
			 *
			 * @var bool $added_payment_method
			 */
			private static $added_payment_method = false;

			/**
			 * Updated meta boxes
			 *
			 * @var bool $updated_meta_boxes
			 */
			private static $updated_meta_boxes = false;

			/**
			 * Woocommerce order
			 *
			 * @var object $wc_order
			 */
			protected $wc_order;

			/**
			 * Main function
			 */
			public function __construct() {
				$this->payment_id   = $this->id;
				$this->language     = Genericshop_General_Functions::grs_get_shop_language();
				$this->plugins_url  = Genericshop_General_Functions::grs_get_plugin_url();

				$this->form_fields = $this->get_backend_configuration_form_fields();
				$this->method_title = 'Generic Shop - ' . $this->get_title();
				$this->init_settings();

				// Save admin configuration from woocomerce checkout tab.
				add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
				// Frontend hook.
				add_action( 'woocommerce_receipt_' . $this->payment_id, array( &$this, 'receipt_page' ) );
				add_action( 'woocommerce_thankyou_' . $this->payment_id, array( &$this, 'thankyou_page' ) );
				// Backend hook.
				add_action( 'woocommerce_process_shop_order_meta', array( &$this, 'save_order_meta' ) );
				add_action( 'woocommerce_admin_order_data_after_order_details', array( &$this, 'update_order_status' ) );
				add_action( 'woocommerce_admin_order_data_after_billing_address', array( &$this, 'add_payment_method' ) );
				add_action( 'woocommerce_admin_order_data_after_shipping_address', array( &$this, 'add_additional_information' ) );

				// Enable woocommerce refund for {payment gateway}.
				$this->supports = array( 'refunds' );

				if ( isset( WC()->session->genericshop_thankyou_page ) ) {
					unset( WC()->session->genericshop_thankyou_page );
				}
				if ( isset( WC()->session->genericshop_receipt_page ) ) {
					unset( WC()->session->genericshop_receipt_page );
				}
				if ( isset( WC()->session->genericshop_confirmation_page ) ) {
					unset( WC()->session->genericshop_confirmation_page );
				}

			}

			/**
			 * Get payment method backend configuration form fields
			 */
			public function get_backend_configuration_form_fields() {

				$form_fields = array(
					'enabled'      => array(
						'title'  => __( 'BACKEND_CH_ACTIVE', 'wc-genericshop' ),
						'type'    => 'checkbox',
						'default'   => '',
					),
					'server_mode'   => array(
						'title'  => __( 'BACKEND_CH_SERVER', 'wc-genericshop' ),
						'css'   => 'padding: 1px;',
						'type'    => 'select',
						'options'   => array(
							'TEST' => __( 'BACKEND_CH_MODE_TEST', 'wc-genericshop' ),
							'LIVE' => __( 'BACKEND_CH_MODE_LIVE', 'wc-genericshop' ),
						),
						'default'   => 'TEST',
					),
					'channel_id' => array(
						'title'  => __( 'BACKEND_CH_CHANNEL', 'wc-genericshop' ),
						'type'    => 'text',
						'default'   => '',
					),
				);

				return $form_fields;
			}

			/**
			 * Get Payment Type from backend configuration settings
			 * if not isset get from class attribute payment_type
			 *
			 * @return string
			 */
			public function get_payment_type() {

				if ( isset( $this->settings['trans_mode'] ) ) {
					return $this->settings['trans_mode'];
				}

				return $this->payment_type;
			}

			/**
			 * Get Payment Brand from backend configuration settings
			 * if not isset get from class attribute payment_brand
			 *
			 * @return string
			 */
			public function get_payment_brand() {

				if ( isset( $this->settings['card_types'] ) && is_array( $this->settings['card_types'] ) ) {
					return strtoupper( implode( ' ', $this->settings['card_types'] ) );
				}

				return $this->payment_brand;
			}

			/**
			 * Get Payment Group from class attribute payment_group
			 *
			 * @return string
			 */
			public function get_payment_group() {

				return $this->payment_group;
			}

			/**
			 * Return true/false if payment method is recuring
			 *
			 * @return bool
			 */
			public function is_recurring() {

				return $this->is_recurring;
			}

			/**
			 * Get Credentials from Database
			 *
			 * @param bool $is_testmode_available is testmode available.
			 * @param bool $is_multichannel_available is testmode available.
			 * @return array
			 */
			public function get_credentials( $is_testmode_available = true, $is_multichannel_available = false ) {

				$credentials['login'] = get_option( 'genericshop_general_login' );
				$credentials['password'] = get_option( 'genericshop_general_password' );
				$credentials['server_mode'] = $this->settings['server_mode'];

				if ( ! empty( $this->settings['multichannel'] ) && $is_multichannel_available ) {
					$credentials['channel_id']  = $this->settings['channel_moto'];
				} else {
					$credentials['channel_id']  = $this->settings['channel_id'];
				}

				if ( $is_testmode_available ) {
					$credentials['test_mode'] = $this->get_test_mode();
				}

				return $credentials;
			}

			/**
			 * Get Test Mode from class attribue test_mode
			 * Return false if server mode setting is LIVE
			 *
			 * @return boolean | string ( EXTERNAL / INTERNAL )
			 */
			public function get_test_mode() {
				if ( 'LIVE' === $this->settings['server_mode'] ) {
					return false;
				}

				return $this->test_mode;
			}

			/**
			 * Get Payment Logo(s)
			 * ( extended at Generic Shop payment gateways class )
			 *
			 * @return boolean
			 */
			protected function grs_get_payment_logo() {

				return false;
			}

			/**
			 * From class WC_Payment_Gateway
			 * Available payment gateway at frontend.
			 *
			 * @return boolean
			 */
			public function is_available() {

				$is_available = parent::is_available();

				if ( $is_available ) {
					$recurring = get_option( 'genericshop_general_recurring' );
					switch ( $this->payment_id ) {
						case 'genericshop_cc':
							if ( $recurring && 0 < get_current_user_id() ) {
								$is_available = false;
							}
							break;
						case 'genericshop_ccsaved':
							if ( ! $recurring || 0 === get_current_user_id() ) {
								$is_available = false;
							}
							break;
						default:
							$is_available = true;
							break;
					}
				}
				return $is_available;
			}

			/**
			 * Check if current user login is admin
			 *
			 * @return boolean
			 */
			private function is_site_admin() {
				return in_array( 'administrator',  wp_get_current_user()->roles, true );
			}

			/**
			 * Get wc order property value
			 *
			 * @param  string $property property of class WC_Order.
			 * @return mixed
			 */
			protected function get_wc_order_property_value( $property ) {
				if ( Genericshop_General_Functions::is_version_greater_than( '3.0' ) ) {
					$function = 'get_' . $property;
					return $this->wc_order->$function();
				}
				return $this->wc_order->$property;
			}

			/**
			 * Get wc session property value
			 *
			 * @param string $property property of class WC_Session.
			 * @return mixed
			 */
			protected function get_wc_session_property_value( $property ) {
				$session_property_value = WC()->session->get( $property );

				if ( isset( $session_property_value ) ) {
					return $session_property_value;
				}
				return false;
			}

			/**
			 * From class WC_Payment_Gateway
			 * Payment gateway icon.
			 * ( extended at Generic Shop payment gateways class )
			 */
			public function get_icon() {

				$title = $this->get_title();

				$billing_fields_css_script = '<script>$(".woocommerce-billing-fields").css({"min-height": "650px"});</script>';
				$icon_html = '<img src="' . $this->grs_get_payment_logo() . '" alt="' . $title . '" title="' . $title . '" style="height:40px; margin:5px 10px 5px 0; float: none; vertical-align: middle; position: inherit;" />';

				return apply_filters( 'woocommerce_gateway_icon', $billing_fields_css_script . $icon_html, $this->id );
			}

			/**
			 * Get multi icon ( brands ) such as Credit card
			 *
			 * @return string
			 */
			public function grs_get_multi_icon() {
				$icon_html = '';

				$payment_setting = get_option( 'woocommerce_' . $this->payment_id . '_settings' );
				$cards = $payment_setting['card_types'];
				if ( isset( $cards ) && '' !== $cards ) {
					foreach ( $cards as $card ) {
						$icon = $this->plugins_url . '/assets/images/' . strtolower( $card ) . '.png';
						$icon_html .= '<img src="' . $icon . '" alt="' . strtolower( $card ) . '" title="' . strtolower( $card ) . '" style="height:40px; margin:5px 10px 5px 0; float: none; vertical-align: middle; position: inherit;" />';
					}
				}

				return $icon_html;
			}

			/**
			 * Process the payment and return the result
			 *
			 * @param int $order_id order id.
			 * @return array
			 */
			public function process_payment( $order_id ) {
				$order = new WC_Order( $order_id );
				return array(
					'result'   => 'success',
					'redirect' => $order->get_checkout_payment_url( true ),
				);
			}

			/**
			 * Create payment gateways form ( form / redirect )
			 * Calls from hook "woocommerce_receipt_{gateway_id}"
			 *
			 * @param int $order_id order id.
			 */
			public function receipt_page( $order_id ) {
				$accept_payment = Genericshop_General_Functions::grs_get_request_value( 'accept_payment' );
				$cancel_payment = Genericshop_General_Functions::grs_get_request_value( 'cancel_payment' );
				$confirmation_page = Genericshop_General_Functions::grs_get_request_value( 'confirmation_page' );

				if ( Genericshop_General_Functions::grs_get_request_value( 'id' ) && empty( $accept_payment ) &&
					empty( $confirmation_page ) ) {
					$this->grs_response_page( $order_id );
				} elseif ( ! empty( $accept_payment ) ) {
					$this->grs_accept_payment( $order_id );
				} elseif ( ! empty( $cancel_payment ) ) {
					$this->grs_cancel_payment( $order_id );
				}

				if ( Genericshop_General_Functions::grs_get_request_value( 'confirmation_page' )  && ! isset( WC()->session->genericshop_confirmation_page ) ) {
					$this->grs_confirmation_page( $order_id );
					WC()->session->set( 'genericshop_confirmation_page', true );
				} elseif ( ! Genericshop_General_Functions::grs_get_request_value( 'confirmation_page' ) && ! isset( WC()->session->genericshop_receipt_page ) ) {
					$this->grs_set_payment_form( $order_id );
					WC()->session->set( 'genericshop_receipt_page', true );
				}
			}

			/**
			 * Set and load the payment checkout form
			 *
			 * @param int $order_id order id.
			 */
			private function grs_set_payment_form( $order_id ) {
				global $wp;
				$is_one_click_payments = false;

				if ( get_option( 'genericshop_general_dob_gender' ) ) {
					$billing_dob = $this->get_wc_session_property_value( 'generic_billing_dob' );

					$is_dob_lower_than_today = Genericshop_General_Functions::grs_is_date_of_birth_lower_than_today( $billing_dob );
					$is_dob_valid = Genericshop_General_Functions::grs_is_date_of_birth_valid( $billing_dob );
					if ( ! get_option( 'genericshop_general_dob_gender' ) || ! $is_dob_valid ) {
						$this->grs_do_error_payment( $order_id, 'wc-failed', 'ERROR_WRONG_DOB' );
					}
				}
				Genericshop_General_Functions::add_log( 'Get checkout parameters' );

				$checkout_id = '';
				$payment_parameters = $this->grs_set_payment_parameters( $order_id );

				$checkout_result = GenericshopPaymentCore::get_checkout_result( $payment_parameters );
				if ( $checkout_result['is_valid'] ) {
					Genericshop_General_Functions::add_log( 'Get payment widget url' );

					$url_config['payment_widget'] = GenericshopPaymentCore::get_payment_widget_url( $payment_parameters['server_mode'], $checkout_result['response']['id'] );
				}

				if ( ! $checkout_result['is_valid'] ) {
					$this->grs_do_error_payment( $order_id, 'wc-canceled', $checkout_result['response'] );
				} elseif ( ! isset( $checkout_result['response']['id'] ) ) {
					$this->grs_do_error_payment( $order_id, 'wc-canceled', 'ERROR_GENERAL_REDIRECT' );
				}

				if ( isset( $wp->request ) ) {
					$url_config['return_url'] = $this->grs_get_home_url( $wp->request ) . 'key=' . Genericshop_General_Functions::grs_get_request_value( 'key' );
				} else {
					$url_config['return_url'] = get_page_link() . '&order-pay=' . Genericshop_General_Functions::grs_get_request_value( 'order-pay' ) . '&key=' . Genericshop_General_Functions::grs_get_request_value( 'key' );
				}

				$url_config['cancel_url'] = $this->get_wc_checkout_url();

				if ( ! empty( $payment_parameters['registrations'] ) ) {
					$is_one_click_payments = true;
				}

				$payment_widget_content = GenericshopPaymentCore::get_payment_widget_content( $url_config['payment_widget'], $payment_parameters['server_mode'] );

				if ( ! $payment_widget_content['is_valid'] || strpos( $payment_widget_content['response'], 'errorDetail' ) !== false ) {
					$this->grs_do_error_payment( $order_id, 'wc-canceled', 'ERROR_GENERAL_REDIRECT' );
				}

				$payment_form = Genericshop_General_Functions::grs_get_payment_form( $this->payment_id );
				$args = array(
					'url_config' => $url_config,
					'payment_parameters' => $payment_parameters,
					'plugins_url' => $this->plugins_url,
					'is_one_click_payments' => $is_one_click_payments,
					'is_recurring' => $this->is_recurring,
					'settings' => $this->settings,
					'payment_method' => $this->payment_id,
					'merchant_location' => get_option( 'genericshop_general_merchant_location' ),
				);

				wp_enqueue_script( 'genericshop_formpayment_script', $url_config['payment_widget'], array(), null );
				wp_enqueue_style( 'genericshop_formpayment_style', $this->plugins_url . '/assets/css/formpayment.css', array(), null );

				Genericshop_General_Functions::grs_include_template( dirname( __FILE__ ) . '/templates/checkout/template-payment-form.php', $args );
			}

			/**
			 * Get wc checkout url
			 *
			 * @return string
			 */
			protected function get_wc_checkout_url() {
				if ( Genericshop_General_Functions::is_version_greater_than( '3.0' ) ) {
					return wc_get_checkout_url();
				} else {
					return WC()->cart->get_checkout_url();
				}
			}

			/**
			 * Return base url
			 *
			 * @param string $wp_request_type get url delimeter.
			 * @return string
			 */
			private function grs_get_home_url( $wp_request_type ) {
				if ( false !== strpos( home_url( $wp_request_type ), '/?' ) ) {
					$home_url = home_url( $wp_request_type ) . '&';
				} else {
					$home_url = home_url( $wp_request_type ) . '/?';
				}
				return $home_url;
			}

			/**
			 * Get payment response page
			 *
			 * @param int $order_id order id.
			 */
			private function grs_response_page( $order_id ) {
				$is_testmode_available = false;
				$status_parameters = $this->get_credentials( $is_testmode_available );

				$id = Genericshop_General_Functions::grs_get_request_value( 'id' );

				$payment_result = GenericshopPaymentCore::get_payment_status( $id, $status_parameters );

				Genericshop_General_Functions::add_log( 'Payment result ', $payment_result, $order_id );

				if ( ! $payment_result['is_valid'] ) {
					$this->grs_do_error_payment( $order_id, 'wc-failed', $payment_result['response'], 'ERROR_GENERAL_NORESPONSE' );
				} else {
					if ( isset( $payment_result['response']['amount'] ) ) {
						$payment_amount = $payment_result['response']['amount'];
					}

					$result_status = GenericshopPaymentCore::get_transaction_result( $payment_result['response']['result']['code'] );

					if ( 'ACK' === $result_status ) {
						if ( $this->is_recurring ) {
							$account = Genericshop_General_Functions::grs_get_account_by_result( $this->payment_id, $payment_result['response'] );

							Genericshop_General_Functions::add_log( 'Payment is success' );

							$this->grs_save_recurring_payment( $order_id, $payment_result['response'], $account );
						}
						$payment_result['response']['payment_status'] = $this->grs_get_success_payment_status( $this->payment_id, $payment_result['response']['result']['code'] );

						Genericshop_General_Functions::add_log( 'Payment is success' );

						$this->grs_do_success_payment( $order_id, $payment_result['response'] );
					} else {
						Genericshop_General_Functions::add_log( 'Payment is failed' );

						$this->grs_failed_response( $order_id, $payment_result['response'], $result_status );
					}
				}// End if().
			}

			/**
			 * Accept payment after confirmation
			 *
			 * @param int $order_id order id.
			 */
			private function grs_cancel_payment( $order_id ) {
				$this->grs_do_error_payment( $order_id, 'wc-cancelled','ERROR_GENERAL_CANCEL' );
			}

			/**
			 * Run after payment successful
			 *
			 * @param int   $order_id order id.
			 * @param array $payment_result payment result.
			 */
			private function grs_do_success_payment( $order_id, $payment_result ) {

				$order = new WC_Order( $order_id );
				$merchant_info = $this->grs_get_merchant_info();

				if ( Genericshop_General_Functions::grs_is_version_tracker_active() ) {
					GenericshopVersionTracker::send_version_tracker( $merchant_info );
				}

				$reference_id = $payment_result['id'];
				$this->grs_save_transactions( $order_id, $payment_result, $reference_id );

				// Update Order Status.
				$order->update_status( 'wc-processing', 'order_note' );
				$order->update_status( $payment_result['payment_status'], 'order_note' );
				$order_awaiting_payment_session = WC()->session->order_awaiting_payment;
				// Empty awaiting payment session.
				if ( ! empty( $order_awaiting_payment_session ) ) {
					unset( WC()->session->order_awaiting_payment );
				}
				// Reduce stock levels.
				if ( Genericshop_General_Functions::is_version_greater_than( '3.0' ) ) {
					wc_reduce_stock_levels( $order_id );
				} else {
					$order->reduce_order_stock();
				}
				// Remove cart.
				WC()->cart->empty_cart();
				wp_safe_redirect( $this->get_return_url( $order ) );
				exit();
			}

			/**
			 * Run after payment response failed
			 *
			 * @param int    $order_id order id.
			 * @param array  $payment_result payment result.
			 * @param string $result_status result status.
			 */
			private function grs_failed_response( $order_id, $payment_result, $result_status ) {
				if ( 'NOK' === $result_status ) {
					$error_identifier = GenericshopPaymentCore::get_error_identifier( $payment_result['result']['code'] );
				} else {
					$error_identifier = 'ERROR_UNKNOWN';
				}

				$payment_result['payment_status'] = 'wc-failed';
				$this->grs_save_transactions( $order_id, $payment_result, $payment_result['id'] );
				$this->grs_do_error_payment( $order_id, $payment_result['payment_status'], $error_identifier );
			}

			/**
			 * Error payment action
			 *
			 * @param int          $order_id order id.
			 * @param string       $payment_status payment status.
			 * @param string|array $error_identifier error identifier.
			 * @param string       $error_default error default.
			 */
			private function grs_do_error_payment( $order_id, $payment_status, $error_identifier, $error_default = 'ERROR_UNKNOWN' ) {
				global $woocommerce;

				$order = new WC_Order( $order_id );

				if ( 'ERROR_UNKNOWN' === $error_identifier ) {
					$error_identifier = $error_default;
				}

				$error_translated = Genericshop_General_Functions::grs_translate_error_identifier( $error_identifier );

				// Cancel the order.
				$order->update_status( $error_translated );
				$order->update_status( $payment_status, 'order_note' );

				// To display failure messages from woocommerce session.
				if ( isset( $error_translated ) ) {
					$woocommerce->session->errors = $error_translated;
					wc_add_notice( $error_translated, 'error' );
				}

				wp_safe_redirect( $this->get_wc_checkout_url() );
				exit();
			}

			/**
			 * Thankyou page
			 * Calls from hook "woocommerce_thankyou_{gateway_id}"
			 */
			public function thankyou_page() {
				if ( ! isset( WC()->session->genericshop_thankyou_page ) ) {
					WC()->session->set( 'genericshop_thankyou_page', true );
				}
			}

			/**
			 * [BACKEND] Save order from meta boxes
			 * Change payment status & save backend order
			 * Calls from hook "woocommerce_process_shop_order_meta"
			 */
			public function save_order_meta() {
				if ( ! self::$saved_meta_boxes ) {
					$original_post_status = Genericshop_General_Functions::grs_get_request_value( 'original_post_status', '' );
					$auto_draft = Genericshop_General_Functions::grs_get_request_value( 'auto_draft', false );

					if ( 'auto-draft' === $original_post_status && '1' === $auto_draft ) {
						$this->grs_save_backend_order();
					} else {
						$order_id = Genericshop_General_Functions::grs_get_request_value( 'post_ID', '' );
						$is_genericshop_payment = $this->grs_is_genericshop_by_order( $order_id );
						if ( $is_genericshop_payment ) {
							$this->grs_change_payment_status();
						}
					}
					self::$saved_meta_boxes = true;
				}
			}

			/**
			 * [BACKEND] Change payment status at backend
			 */
			private function grs_change_payment_status() {
				$order_id = Genericshop_General_Functions::grs_get_request_value( 'post_ID', '' );
				$original_post_status = Genericshop_General_Functions::grs_get_request_value( 'original_post_status', '' );
				$order_post_status = Genericshop_General_Functions::grs_get_request_value( 'order_status', '' );

				if ( 'wc-in-review' === $original_post_status && 'wc-in-review' === $order_post_status ) {
					$this->grs_update_payment_status( $order_id, $original_post_status );
				} elseif ( 'wc-in-review' === $original_post_status && ( 'wc-failed' === $order_post_status || 'wc-cancelled' === $order_post_status ) ) {
					$this->grs_update_payment_status( $order_id, $original_post_status, $order_post_status );
				} elseif ( 'wc-pre-authorization' === $original_post_status && 'wc-payment-accepted' === $order_post_status ) {
					$backoffice_config['payment_type'] = 'CP';
					$backoffice_config['order_status'] = $order_post_status;
					$backoffice_config['error_message'] = __( 'ERROR_GENERAL_CAPTURE_PAYMENT', 'wc-genericshop' );

					$this->grs_do_back_office_payment( $order_id, $backoffice_config );
				} elseif ( 'wc-payment-accepted' === $original_post_status && 'wc-refunded' === $order_post_status ) {
					$this->wc_order = new WC_Order( $order_id );
					$this->increase_order_stock();
					$backoffice_config['payment_type'] = 'RF';
					$backoffice_config['order_status'] = $order_post_status;
					$backoffice_config['error_message'] = __( 'ERROR_GENERAL_REFUND_PAYMENT', 'wc-genericshop' );

					$this->grs_do_back_office_payment( $order_id, $backoffice_config );
				} elseif ( 'wc-payment-accepted' !== $original_post_status && 'wc-refunded' === $order_post_status ) {
					$redirect = get_admin_url() . 'post.php?post=' . $order_id . '&action=edit';
					wp_safe_redirect( $redirect );
					exit;
				}

			}

			/**
			 * Check if order is using Generic Shop Payment module
			 * validate payment module from order id
			 *
			 * @param int $order_id order id.
			 * @return boolean
			 */
			private function grs_is_genericshop_by_order( $order_id ) {
				$transaction_log = Genericshop_General_Models::grs_get_db_transaction_log( $order_id );
				$payment_id = $transaction_log['payment_id'];

				return $this->grs_is_genericshop_by_payment_id( $payment_id );
			}

			/**
			 * Check if payment method is using Generic Shop Payment
			 *
			 * @param string $payment_method payment method.
			 * @return boolean
			 */
			private function grs_is_genericshop_by_payment_id( $payment_method ) {
				if ( false !== strpos( $payment_method, 'genericshop' ) ) {
					return true;
				}
				return false;
			}

			/**
			 * [BACKEND] Update payment status from gateway
			 *
			 * @param int    $order_id order id.
			 * @param string $original_post_status original post status.
			 * @param string $order_post_status order post status.
			 */
			private function grs_update_payment_status( $order_id, $original_post_status = false, $order_post_status = false ) {
				$transaction_log = Genericshop_General_Models::grs_get_db_transaction_log( $order_id );

				if ( ! $original_post_status ) {
					$original_post_status = $transaction_log['payment_status'];
				}

				$payment_gateway = wc_get_payment_gateway_by_order( $order_id );
				$backoffice_parameter = $payment_gateway->get_credentials();

				$update_status_response = GenericshopPaymentCore::update_status( $transaction_log['reference_id'],$backoffice_parameter );

				if ( ! $update_status_response['is_valid'] ) {
					Genericshop_General_Functions::add_log( 'update status is failed with error : ' . $update_status_response['response'] );
					return false;
				} else {
					$backoffice_result = simplexml_load_string( $update_status_response['response'] );
					$backoffice_result_code = (string) $backoffice_result->Result->Transaction->Processing->Return['code'];
					$transaction_result = GenericshopPaymentCore::get_transaction_result( $backoffice_result_code );

					if ( 'ACK' === $transaction_result && ! GenericshopPaymentCore::is_success_review( $backoffice_result_code ) ) {
						$payment_code = (string) $backoffice_result->Result->Transaction->Payment['code'];
						$payment_type = substr( $payment_code, -2 );
						if ( 'PA' === $payment_type ) {
							$payment_status = 'wc-pre-authorization';
						} elseif ( 'DB' === $payment_type ) {
							$payment_status = 'wc-payment-accepted';
						}

						Genericshop_General_Models::grs_update_db_transaction_log_status( $order_id, $payment_status );
						Genericshop_General_Models::grs_update_db_posts_status( $order_id, $payment_status );
						if ( $original_post_status !== $payment_status ) {
							if ( $order_post_status && ( 'wc-failed' === $order_post_status || 'wc-cancelled' === $order_post_status ) ) {
								$this->wc_order = new WC_Order( $order_id );
								$this->increase_order_stock();
							}
							$wc_order_status = wc_get_order_statuses();
							$status_name['original'] = $wc_order_status[ $original_post_status ];
							$status_name['new'] = $wc_order_status[ $payment_status ];
							$this->grs_add_order_notes( $order_id, $status_name );
						}
						Genericshop_General_Functions::add_log( 'update status is success' );
						return true;
					} else {
						Genericshop_General_Functions::add_log( 'update status is failed' );
						return false;
					}
				}// End if().
			}

			/**
			 * [BACKEND] Save backend order
			 * Add order from backend with registered payment
			 */
			private function grs_save_backend_order() {
				$order['order_id'] = Genericshop_General_Functions::grs_get_request_value( 'post_ID', '' );
				$reg_id = Genericshop_General_Functions::grs_get_request_value( '_payment_recurring', '' );
				$payment_method = Genericshop_General_Functions::grs_get_request_value( '_payment_method', '' );

				$is_genericshop_payment = $this->grs_is_genericshop_by_payment_id( $payment_method );

				if ( $is_genericshop_payment ) {
					$registered_payment = Genericshop_General_Models::grs_get_db_registered_payment_by_regid( $reg_id );
					$order['payment_id'] = Genericshop_General_Functions::grs_get_payment_id_by_group( $registered_payment['payment_group'] );
					$order['user_id'] = $registered_payment['cust_id'];
					$order['payment_brand'] = $registered_payment['brand'];

					$order_parameter = $this->grs_get_backend_order_parameters( $order );

					Genericshop_General_Functions::add_log( 'backend order parameters ', $order_parameter, $order['order_id'] );

					$order_result = GenericshopPaymentCore::use_registered_account( $reg_id, $order_parameter );

					Genericshop_General_Functions::add_log( 'backend order result ', $order_result );

					if ( ! $order_result['is_valid'] ) {
						$order['payment_status'] = 'wc-failed';
						Genericshop_General_Models::grs_update_db_posts_status( $order['order_id'], $order['payment_status'] );

						Genericshop_General_Functions::add_log( 'backend order is not success with error : ' . $order_result['response'] );
					} else {
						$transaction_result = GenericshopPaymentCore::get_transaction_result( $order_result['response']['result']['code'] );

						if ( 'ACK' === $transaction_result ) {
							$order['payment_status'] = $this->grs_get_success_payment_status( $order['payment_id'], $order_result['response']['result']['code'] );
							$order['reference_id'] = $order_result['response']['id'];
							$this->grs_do_success_backend_order( $order, $order_parameter );
							Genericshop_General_Functions::add_log( 'backend order is success' );
						} else {
							$order['payment_status'] = 'wc-failed';
							Genericshop_General_Models::grs_update_db_posts_status( $order['order_id'], $order['payment_status'] );

							Genericshop_General_Functions::add_log( 'backend order is not success' );
						}
					}

					$_POST['order_status'] = $order['payment_status'];
					$_POST['_payment_method'] = $order['payment_id'];
				}// End if().
			}

			/**
			 * [BACKEND] Get backend order parameters
			 * Get parameters for backend order
			 *
			 * @param array $order order.
			 * @return array
			 */
			private function grs_get_backend_order_parameters( $order ) {
				$order_parameters = array();

				$payment_gateway = Genericshop_General_Functions::grs_get_payment_gateway_by_id( $order['payment_id'] );
				$order_parameters = $payment_gateway->get_credentials();

				$order_detail = Genericshop_General_Models::grs_get_db_order_detail( $order['order_id'] );
				foreach ( $order_detail as $value ) {
					if ( '_order_total' === $value['meta_key'] ) {
						$order_parameters['amount'] = $value['meta_value'];
					} elseif ( '_order_currency' === $value['meta_key'] ) {
						$order_parameters['currency'] = $value['meta_value'];
					}
				}

				$order_parameters['transaction_id'] = $order['order_id'];
				$order_parameters['payment_type'] = $payment_gateway->get_payment_type();
				$order_parameters['payment_recurring'] = 'REPEATED';

				return $order_parameters;
			}

			/**
			 * [BACKEND] Run when backend order success
			 *
			 * @param array $order order.
			 * @param array $order_parameter order parameter.
			 */
			private function grs_do_success_backend_order( $order, $order_parameter ) {
				$this->grs_add_customer_note( $order['payment_id'], $order['order_id'] );
				$this->wc_order = new WC_Order( $order['order_id'] );

				$payment_gateway = Genericshop_General_Functions::grs_get_payment_gateway_by_id( $order['payment_id'] );
				$order['payment_type'] = $payment_gateway->get_payment_type();
				$order['amount'] = $order_parameter['amount'];
				$order['currency'] = $order_parameter['currency'];
				if ( isset( $order_parameter['transaction_id'] ) ) {
					$order['transaction_id'] = $order_parameter['transaction_id'];
					$order['customer_id'] = ( $this->wc_order->get_user_id() ) ? $this->wc_order->get_user_id()  : 0;
				}
				Genericshop_General_Models::grs_save_db_transaction( $order );
				Genericshop_General_Models::grs_update_db_posts_status( $order['order_id'], $order['payment_status'] );
			}

			/**
			 * [BACKEND] From class WC_Payment_Gateway
			 * Payment refund from button refund.
			 *
			 * @param int    $order_id order id.
			 * @param float  $amount amount.
			 * @param string $reason reason.
			 * @return boolean
			 */
			public function process_refund( $order_id, $amount = null, $reason = '' ) {
				$is_genericshop_payment = $this->grs_is_genericshop_by_order( $order_id );

				if ( $is_genericshop_payment ) {
					$woocommerce_refund = true;
					$backoffice_config['payment_type'] = 'RF';
					$backoffice_config['order_status'] = 'wc-refunded';
					$backoffice_config['error_message'] = __( 'ERROR_GENERAL_REFUND_PAYMENT', 'wc-genericshop' );

					$refund_result = $this->grs_do_back_office_payment( $order_id, $backoffice_config, $woocommerce_refund );

					if ( $refund_result ) {
						$order = new WC_Order( $order_id );
						$order->update_status( 'wc-refunded', 'order_note' );

						$args = array(
							'post_parent' => $order_id,
							'post_type' => 'shop_order_refund',
						);

						$child_posts = get_children( $args );
						if ( Genericshop_General_Functions::is_version_greater_than( '3.0' ) ) {
							$posts = $this->search_array_by_postmeta( $child_posts );
						} else {
							$posts = $this->search_array_by_value( 'Order Fully Refunded', $child_posts );
						}

						if ( count( $posts ) > 0 ) {
							wp_delete_post( $posts->ID, true );
						}

						Genericshop_General_Models::grs_update_db_transaction_log_status( $order_id, 'wc-refunded' );
					}
					return $refund_result;
				}
				return false;
			}

			/**
			 * Search array by postmeta from multidimensional array
			 *
			 * @param  array $array array data.
			 * @return array | null
			 */
			public function search_array_by_postmeta( $array ) {
				foreach ( $array as $key => $val ) {
					if ( strpos( get_post_meta( $val->ID, '_refund_reason', true ), 'Bestellung vollständig' ) !== false
						|| strpos( get_post_meta( $val->ID, '_refund_reason', true ), 'Order fully' ) !== false ) {
						return $val;
					}
				}

				return null;
			}

			/**
			 * Search array by value from multidimensional array
			 *
			 * @param  string $value words you want to search for.
			 * @param  array  $array array data.
			 * @return array | null
			 */
			public function search_array_by_value( $value, $array ) {
				foreach ( $array as $key => $val ) {
					if ( $val->post_excerpt === $value ) {
						return $val;
					}
				}
				return null;
			}

			/**
			 * [BACKEND] Increase order stock
			 * increase order stock and add note to order detail
			 * used when refund
			 */
			protected function increase_order_stock() {
				$items = $this->wc_order->get_items();
				foreach ( $items as $item ) {
					$wc_product = $this->wc_order->get_product_from_item( $item );
					$is_managed_stock = $this->is_managed_stock( $item['product_id'] );
					if ( $is_managed_stock ) {
						$product_stock = $wc_product->get_stock_quantity();
						$wc_product->increase_stock( $item['qty'] );
						$order_note = 'Item #' . $item['product_id'] .
							' stock increased from ' . $product_stock . ' to ' . ( $product_stock + 1 );
						$this->wc_order->add_order_note( $order_note );
					}
				}
			}

			/**
			 * [BACKEND] Check if the product stock is manageable
			 *
			 * @param int $product_id product id.
			 * @return bool
			 */
			protected function is_managed_stock( $product_id ) {
				$is_managed_stock = get_post_meta( $product_id, '_manage_stock', true );
				if ( 'yes' === $is_managed_stock ) {
					return true;
				}
				return false;
			}

			/**
			 * [BACKEND] Capture / Refund Payment
			 * do back office operations ( capture/refund )
			 *
			 * @param int   $order_id order id.
			 * @param array $back_office_config back office config.
			 * @param bool  $woocommerce_refund woocommerce refund.
			 * @return boolean ( if @param $woocommerce_refund = true )
			 */
			private function grs_do_back_office_payment( $order_id, $back_office_config, $woocommerce_refund = false ) {
				$transaction_log = Genericshop_General_Models::grs_get_db_transaction_log( $order_id );
				$is_testmode_available = true;
				$is_multichannel_available = true;
				$amount = Genericshop_General_Functions::grs_get_request_value( 'refund_amount', false );

				$payment_gateway = wc_get_payment_gateway_by_order( $order_id );
				$backoffice_parameter = $payment_gateway->get_credentials( $is_testmode_available, $is_multichannel_available );

				if ( $woocommerce_refund ) {
					$backoffice_parameter['amount'] = $amount;
				} else {
					$backoffice_parameter['amount'] = $transaction_log['amount'];
				}
				$backoffice_parameter['currency'] = $transaction_log['currency'];
				$backoffice_parameter['payment_type'] = $back_office_config['payment_type'];

				Genericshop_General_Functions::add_log( 'Back office operation parameters ', $backoffice_parameter, $order_id );

				$backoffice_result = GenericshopPaymentCore::back_office_operation( $transaction_log['reference_id'], $backoffice_parameter );

				Genericshop_General_Functions::add_log( 'Back office operation result ', $backoffice_result , $order_id );

				if ( ! $backoffice_result['is_valid'] ) {
					if ( 'ERROR_UNKNOWN' !== $backoffice_result['response'] ) {
						$back_office_config['error_message'] = Genericshop_General_Functions::grs_translate_error_identifier( $backoffice_result['response'] );
					}
					if ( $woocommerce_refund ) {
						return false;
					} else {
						$this->grs_backend_redirect_error( $order_id, $back_office_config['error_message'] );
					}
				} else {
					$transaction_result = GenericshopPaymentCore::get_transaction_result( $backoffice_result['response']['result']['code'] );
					if ( 'ACK' === $transaction_result ) {
						Genericshop_General_Models::grs_update_db_transaction_log_status( $order_id, $back_office_config['order_status'] );

						Genericshop_General_Functions::add_log( 'Back office operation is success' );
						if ( $woocommerce_refund ) {
							return true;
						}
					} else {
						if ( $woocommerce_refund ) {
							return false;
						} else {
							$this->grs_backend_redirect_error( $order_id, $back_office_config['error_message'] );
						}
					}
				}
			}

			/**
			 * Backend redirect error
			 *
			 * @param int    $order_id order id.
			 * @param string $error_message error message.
			 */
			protected function grs_backend_redirect_error( $order_id, $error_message ) {
				$redirect = get_admin_url() . 'post.php?post=' . $order_id . '&action=edit';
				WC_Admin_Meta_Boxes::add_error( $error_message );
				wp_safe_redirect( $redirect );
				exit;
			}

			/**
			 * [BACKEND] Add additional information
			 * add additional information on Edit Order Page
			 * Calls from hook "woocommerce_admin_order_data_after_shipping_address"
			 */
			public function add_additional_information() {
				if ( ! self::$added_meta_boxes ) {
					$additional_information = '';
					$order_id = Genericshop_General_Functions::grs_get_request_value( 'post', false );

					$is_genericshop_payment = $this->grs_is_genericshop_by_order( $order_id );
					if ( $is_genericshop_payment ) {
						$transaction_log = Genericshop_General_Models::grs_get_db_transaction_log( $order_id );
						if ( isset( $transaction_log['additional_information'] ) ) {
							$additional_information = $this->grs_set_additional_info( $transaction_log['additional_information'] );
						}
						$payment_gateway = wc_get_payment_gateway_by_order( $order_id );
						$args = array(
							'payment_method_title' => $payment_gateway->get_title(),
							'transaction_log' => $transaction_log,
							'additional_information' => $additional_information,
						);
						Genericshop_General_Functions::grs_include_template( dirname( __FILE__ ) . '/templates/admin/meta-boxes/template-additional-information.php', $args );
					}
					self::$added_meta_boxes = true;
				}
			}

			/**
			 * [BACKEND] Update order status
			 * update order status from gateway
			 * Calls from hook "woocommerce_admin_order_data_after_order_details"
			 */
			public function update_order_status() {
				$post_type = false;
				if ( isset( $_GET['post_type'] ) ) { // input var okay.
					$post_type = wp_verify_nonce( sanitize_text_field( wp_unslash( $_GET['post_type'] ) ) ); // input var okay.
				}

				if ( ! self::$updated_meta_boxes && 'shop_order' !== $post_type ) {
					$order_id = Genericshop_General_Functions::grs_get_request_value( 'post', false );
					$is_genericshop_payment = $this->grs_is_genericshop_by_order( $order_id );

					if ( $is_genericshop_payment ) {
						$order = wc_get_order( $order_id );
						if ( 'in-review' === $order->get_status() ) {
							$request_section = Genericshop_General_Functions::grs_get_request_value( 'section', false );

							if ( $order_id && 'update-order' === $request_section ) {
								$this->grs_update_payment_status( $order_id, 'wc-in-review' );
								$redirect = get_admin_url() . 'post.php?post=' . $order_id . '&action=edit';
								wp_safe_redirect( $redirect );
								exit;
							}
							$args = array(
								'order_id' => $order_id,
							);
							Genericshop_General_Functions::grs_include_template( dirname( __FILE__ ) . '/templates/admin/meta-boxes/template-update-order.php', $args );
						}
					}
					self::$updated_meta_boxes = true;
				}
			}

			/**
			 * [BACKEND] Add payment method
			 * Add Payment Method ( Recurring ) at Backend Order
			 * Calls from hook "woocommerce_admin_order_data_after_billing_address"
			 */
			public function add_payment_method() {
				if ( ! self::$added_payment_method ) {
					$action = Genericshop_General_Functions::grs_get_request_value( 'action', 'false' );
					$order_id = Genericshop_General_Models::grs_get_db_last_order_id();
					if ( $order_id ) {
						$args = array(
							'action' => $action,
						);
						Genericshop_General_Functions::grs_include_template( dirname( __FILE__ ) . '/templates/admin/meta-boxes/template-add-payment-method.php', $args );
					}

					self::$added_payment_method = true;
				}
			}

			/**
			 * Get success payment status
			 *
			 * @param string $payment_id payment id.
			 * @param string $payment_result_code payment result code.
			 * @return string
			 */
			private function grs_get_success_payment_status( $payment_id, $payment_result_code ) {
				$payment_gateway = Genericshop_General_Functions::grs_get_payment_gateway_by_id( $payment_id );
				$is_success_review = GenericshopPaymentCore::is_success_review( $payment_result_code );

				if ( $is_success_review ) {
					$payment_status = 'wc-in-review';
				} else {
					if ( 'PA' === $payment_gateway->get_payment_type() ) {
						$payment_status = 'wc-pre-authorization';
					} else {
						$payment_status = 'wc-payment-accepted';
					}
				}

				return $payment_status;
			}

			/**
			 * Add customer note
			 *
			 * @param string $payment_id payment id.
			 * @param int    $order_id order id.
			 */
			private function grs_add_customer_note( $payment_id, $order_id ) {
				$this->wc_order = new WC_Order( $order_id );

				$customer_note   = $this->get_wc_order_property_value( 'customer_note' );

				$new_line = "\n";
				$transaction_id_title = __( 'BACKEND_TT_TRANSACTION_ID', 'wc-genericshop' );

				$payment_comments  = $this->get_title() . $new_line;

				if ( $customer_note ) {
					$customer_note .= $new_line;
				}

				$customer_note .= html_entity_decode( $payment_comments, ENT_QUOTES, 'UTF-8' );
				$order_notes = array(
					'ID'            => $this->get_wc_order_property_value( 'id' ),
					'post_excerpt'  => $customer_note,
				);
				wp_update_post( $order_notes );
			}

			/**
			 * Get cart items parameters
			 *
			 * @return array
			 */
			private function grs_set_cart_items_parameters() {
				global $woocommerce;
				$wc_cart = $woocommerce->cart;
				$count = 0;
				$cart_items = array();
				foreach ( $wc_cart->get_cart() as $cart ) {
					$cart_items[ $count ]['merchant_item_id'] = $cart['product_id'];
					$cart_items[ $count ]['quantity'] = (int) $cart['quantity'];
					if ( Genericshop_General_Functions::is_version_greater_than( '3.0' ) ) {
						$cart_items[ $count ]['name'] = $cart['data']->get_name();
					} else {
						$cart_items[ $count ]['name'] = $cart['data']->get_title();
					}
					$cart_items[ $count ]['price'] = Genericshop_General_Functions::grs_get_payment_price_with_tax_and_discount( $cart );
					$cart_items[ $count ]['tax'] = Genericshop_General_Functions::grs_get_payment_tax_in_percent( $cart );

					$count++;
				}

				return $cart_items;
			}

			/**
			 * Set customer parameters for order
			 *
			 * @param int $order_id order id.
			 * @return array
			 */
			private function grs_set_customer_parameters( $order_id ) {
				$this->wc_order = new WC_Order( $order_id );

				$customer['first_name']   = $this->get_wc_order_property_value( 'billing_first_name' );
				$customer['last_name']    = $this->get_wc_order_property_value( 'billing_last_name' );
				$customer['email']        = $this->get_wc_order_property_value( 'billing_email' );
				$customer['phone']        = $this->get_wc_order_property_value( 'billing_phone' );
				$customer['birthdate']    = get_option( 'genericshop_general_dob_gender' ) ? date( 'Y-m-d', strtotime( $this->get_wc_session_property_value( 'generic_billing_dob' ) ) ) : '';
				$customer['sex']          = get_option( 'genericshop_general_dob_gender' ) ? Genericshop_General_Functions::grs_get_initial_gender( $this->get_wc_session_property_value( 'generic_billing_gender' ) ) : '';

				return $customer;
			}

			/**
			 * Set billing parameters for order
			 *
			 * @param int $order_id order id.
			 * @return array
			 */
			private function grs_set_billing_parameters( $order_id ) {
				$this->wc_order = new WC_Order( $order_id );

				$billing['street']        = $this->get_wc_order_property_value( 'billing_address_1' );
				if ( trim( $this->get_wc_order_property_value( 'billing_address_2' ) ) ) {
					$billing['street'] .= ', ' . $this->get_wc_order_property_value( 'billing_address_2' );
				}
				$billing['city']          = $this->get_wc_order_property_value( 'billing_city' );
				$billing['zip']           = $this->get_wc_order_property_value( 'billing_postcode' );
				$billing['country_code']  = $this->get_wc_order_property_value( 'billing_country' );

				return $billing;
			}

			/**
			 * Set shipping parameters for order
			 *
			 * @param int $order_id order id.
			 * @return array
			 */
			private function grs_set_shipping_parameters( $order_id ) {
				$this->wc_order = new WC_Order( $order_id );

				$shipping['street']        = $this->get_wc_order_property_value( 'shipping_address_1' );
				if ( trim( $this->get_wc_order_property_value( 'shipping_address_2' ) ) ) {
					$shipping['street'] .= ', ' . $this->get_wc_order_property_value( 'shipping_address_2' );
				}
				$shipping['city']          = $this->get_wc_order_property_value( 'shipping_city' );
				$shipping['zip']           = $this->get_wc_order_property_value( 'shipping_postcode' );
				$shipping['country_code']  = $this->get_wc_order_property_value( 'shipping_country' );

				return $shipping;
			}

			/**
			 * Set recurring parameters for order
			 *
			 * @return array
			 */
			private function grs_set_recurring_parameters() {
				$recurring = array();
				$recurring['registrations'] = $this->grs_set_registration_parameters();
				$recurring['payment_registration'] = 'true';
				$recurring['3D']['amount'] = $this->get_order_total();
				$recurring['3D']['currency'] = get_woocommerce_currency();

				return $recurring;
			}

			/**
			 * Set payment parameters for order
			 *
			 * @param int $order_id order id.
			 * @return array
			 */
			private function grs_set_payment_parameters( $order_id ) {
				$this->wc_order = new WC_Order( $order_id );

				$payment_parameters                             = $this->get_credentials();
				$payment_parameters['transaction_id']           = $this->wc_order->get_order_number();
				$payment_parameters['amount']                   = $this->get_order_total();
				$payment_parameters['currency']                 = get_woocommerce_currency();
				$payment_parameters['customer']                 = $this->grs_set_customer_parameters( $order_id );
				$payment_parameters['billing']                  = $this->grs_set_billing_parameters( $order_id );
				$payment_parameters['payment_type']             = $this->get_payment_type();
				$payment_parameters['payment_brand']            = $this->get_payment_brand();

				if ( $this->is_recurring ) {
					$recurring_parameter = $this->grs_set_recurring_parameters();
					$payment_parameters = array_merge( $payment_parameters, $recurring_parameter );
				}

				Genericshop_General_Functions::add_log( 'Checkout parameters ', $payment_parameters, $order_id );

				$payment_parameters['customer_ip'] = Genericshop_General_Functions::get_customer_ip();

				return $payment_parameters;
			}

			/**
			 * Set additional information
			 *
			 * @param serialize $serialize_info serialize info.
			 * @return array
			 */
			private function grs_set_additional_info( $serialize_info ) {
				$additional_info = false;
				if ( $serialize_info ) {
					$unserialize_info = maybe_unserialize( $serialize_info );
					foreach ( $unserialize_info as $info ) {
						$explode_info = explode( '=>',$info );
						$additional_info[ $explode_info[0] ] = $explode_info[1];
					}
				}

				return $additional_info;
			}

			/**
			 * Set registered payment accounts for one click checkout order
			 *
			 * @return array
			 */
			private function grs_set_registration_parameters() {

				$registration = array();
				$credentials = $this->get_credentials();
				$registered_payments = Genericshop_General_Models::grs_get_db_registered_payment( $this->payment_group, $credentials );

				foreach ( $registered_payments as $key => $registrations ) {
					$registration[ $key ] = $registrations['reg_id'];
				}

				return $registration;
			}

			/**
			 * Save recurring payment from pay and save order
			 *
			 * @param int   $order_id order id.
			 * @param array $payment_result payment result.
			 * @param array $account account.
			 */
			private function grs_save_recurring_payment( $order_id, $payment_result, $account ) {
				$is_registered_payment = Genericshop_General_Models::grs_is_registered_payment_db( $payment_result['registrationId'] );

				if ( ! $is_registered_payment ) {
					$credentials = $this->get_credentials();
					$registered_payment = $account;
					$registered_payment['payment_group'] = $this->payment_group;
					$registered_payment['payment_brand'] = $payment_result['paymentBrand'];
					$registered_payment['server_mode'] = $credentials['server_mode'];
					$registered_payment['channel_id'] = $credentials['channel_id'];
					$registered_payment['registration_id'] = $payment_result['registrationId'];
					$registered_payment['payment_default'] = Genericshop_General_Models::grs_get_db_payment_default( $this->payment_group, $credentials );

					Genericshop_General_Models::grs_save_db_registered_payment( $registered_payment );
				}
			}

			/**
			 * Save order transaction details
			 *
			 * @param int   $order_id order id.
			 * @param array $payment_result payment result.
			 * @param int   $reference_id reference id.
			 */
			private function grs_save_transactions( $order_id, $payment_result, $reference_id ) {
				$this->wc_order = new WC_Order( $order_id );

				$transaction = array();
				$this->grs_add_customer_note( $this->payment_id, $order_id );
				$additional_info = '';

				if ( empty( $payment_result['paymentBrand'] ) ) {
					$payment_result['paymentBrand'] = $this->get_payment_brand();
				}

				$transaction['order_id'] = $this->wc_order->get_order_number();
				$transaction['payment_type'] = $this->get_payment_type();
				$transaction['reference_id'] = $reference_id;
				$transaction['payment_brand'] = $payment_result['paymentBrand'];
				$transaction['transaction_id'] = $payment_result['merchantTransactionId'];
				$transaction['payment_id'] = $this->payment_id;
				$transaction['payment_status'] = $payment_result['payment_status'];
				$transaction['amount'] = $this->get_order_total();
				$transaction['currency'] = get_woocommerce_currency();
				$transaction['customer_id'] = ( $this->wc_order->get_user_id() ) ? $this->wc_order->get_user_id()  : 0;

				Genericshop_General_Models::grs_save_db_transaction( $transaction, $additional_info );
			}

			/**
			 * Add order notes
			 * add order notes ( database : wp_comment ) if change payment status at backend
			 *
			 * @param int   $order_id order id.
			 * @param array $status_name status name.
			 */
			private function grs_add_order_notes( $order_id, $status_name ) {

				$user = get_user_by( 'id', get_current_user_id() );
				/**
				 * $timezone = new DateTimeZone( wc_timezone_string() );
				 * date_default_timezone_set( wc_timezone_string() );
				 */

				$comments['order_id'] = $order_id;
				$comments['author'] = $user->display_name;
				$comments['email'] = $user->user_email;
				$comments['content'] = 'Order status changed from ' . $status_name['original'] . ' to ' . $status_name['new'] . '.';

				Genericshop_General_Models::grs_add_db_order_notes( $comments );

			}

			/**
			 * Get merchants info for version tracker
			 *
			 * @return array
			 */
			public function grs_get_merchant_info() {
				return array_merge(
					$this->grs_get_general_merchant_info(),
					$this->grs_get_credit_card_merchant_info()
				);
			}

			/**
			 * Get general merchants info for version tracker
			 *
			 * @return array
			 */
			protected function grs_get_general_merchant_info() {
				$merchant['transaction_mode'] = $this->settings['server_mode'];
				$merchant['ip_address'] = isset( $_SERVER['SERVER_ADDR'] ) ? sanitize_text_field( wp_unslash( $_SERVER['SERVER_ADDR'] ) ) : ''; // input var okay.
				$merchant['shop_version'] = WC()->version;
				$merchant['plugin_version'] = constant( 'GENERIC_VERSION' );
				$merchant['client'] = 'Genericshop';
				$merchant['merchant_id'] = get_option( 'genericshop_general_merchant_no' );
				$merchant['shop_system'] = 'WOOCOMMERCE';
				$merchant['email'] = get_option( 'genericshop_general_merchant_email' );
				$merchant['shop_url'] = get_option( 'genericshop_general_shop_url' );

				return $merchant;
			}

			/**
			 * Get credit card merchants info for version tracker
			 *
			 * @return array
			 */
			protected function grs_get_credit_card_merchant_info() {
				$merchant = array();

				if ( 'genericshop_cc' === $this->payment_id || 'genericshop_ccsaved' === $this->payment_id ) {
					$merchant['merchant_location'] = get_option( 'genericshop_general_merchant_location' );
				}

				return $merchant;
			}

		} // End of class Genericshop_Payment_Gateway
	}// End if().

	/**
	 * Add Genericshop gateway to WooCommerce
	 *
	 * @access public
	 * @param array $methods methods.
	 * @return array $methods
	 */
	function add_genericshop_payments( $methods ) {
		$methods[] = 'Gateway_Genericshop_CC';
		$methods[] = 'Gateway_Genericshop_CCSaved';
		return $methods;
	}

	add_filter( 'woocommerce_payment_gateways', 'add_genericshop_payments' );
	foreach ( glob( dirname( __FILE__ ) . '/includes/gateways/**.php' ) as $filename ) {
		include_once $filename;
	}
}


add_action( 'wp_ajax_my_action', 'get_ajax_registered_payment' );
/**
 * Get Registered Payment ( AJAX )
 * Handle ajax request from add payment methods at backend order
 * Echo json
 */
function get_ajax_registered_payment() {
	global $wpdb;

	$payment_group = '';
	$user_id = Genericshop_General_Functions::grs_get_request_value( 'user_id' );
	$payment_id = Genericshop_General_Functions::grs_get_request_value( 'payment_id' );

	if ( 'genericshop_ccsaved' === $payment_id ) {
		$payment_group = 'CC';
	}

	if ( isset( $payment_group ) ) {
		$registered_payment = $wpdb->get_results( $wpdb->prepare( "SELECT  * FROM {$wpdb->prefix}genericshop_payment_recurring WHERE cust_id = %d AND payment_group = %s", $user_id, $payment_group ), ARRAY_A ); // db call ok; no-cache ok.
		echo wp_json_encode( $registered_payment );
	}

	wp_die(); // this is required to terminate immediately and return a proper response.
}
