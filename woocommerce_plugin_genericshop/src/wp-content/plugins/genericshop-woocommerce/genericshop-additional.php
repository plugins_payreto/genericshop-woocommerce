<?php
/**
 * Generic Shop eCommerce Plugin Additional
 *
 * This file is used for add payment status, extra customer info.
 * Copyright (c) Generic Shop eCommerce
 *
 * @package Generic Shop eCommerce
 * @located at  /
 */

/**
 * Register new order statuses
 *  ( In Review, Pre-Authorization and Payment Accepted )
 */
function register_payment_status() {

	register_post_status( 'wc-in-review', array(
		'label'                     => _x( 'In Review', 'WooCommerce Order Status', 'wc_genericshop' ),
		'public'                    => true,
		'exclude_from_search'       => false,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		/* translators: %s: https://developer.wordpress.org/rest-api/  */
		'label_count'               => _n_noop( 'In Review ( %s )', 'In Review ( %s )', 'wc_genericshop' ),
	) );

	register_post_status( 'wc-pre-authorization', array(
		'label'                     => _x( 'Pre-Authorization', 'WooCommerce Order Status', 'wc_genericshop' ),
		'public'                    => true,
		'exclude_from_search'       => false,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		/* translators: %s: https://developer.wordpress.org/rest-api/  */
		'label_count'               => _n_noop( 'Pre-Authorization ( %s )', 'Pre-Authorization ( %s )', 'wc_genericshop' ),
	) );

	register_post_status( 'wc-payment-accepted', array(
		'label'                     => _x( 'Payment Accepted', 'WooCommerce Order Status', 'wc_genericshop' ),
		'public'                    => true,
		'exclude_from_search'       => false,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		/* translators: %s: https://developer.wordpress.org/rest-api/  */
		'label_count'               => _n_noop( 'Payment Accepted ( %s )', 'Payment Accepted ( %s )', 'wc_genericshop' ),
	) );

}
add_filter( 'init', 'register_payment_status' );

/**
 * Add new order statuses to woocommerce
 *  ( In Review, Pre-Authorization and Payment Accepted )
 *
 * @param array $order_status order status.
 */
function add_order_status( $order_status ) {

	$order_status['wc-in-review'] = _x( 'In Review', 'In Review Order Status', 'wc_genericshop' );
	$order_status['wc-pre-authorization'] = _x( 'Pre-Authorization', 'Pre-authorization Order Status', 'wc_genericshop' );
	$order_status['wc-payment-accepted'] = _x( 'Payment Accepted', 'Payment Accepted Order Status', 'wc_genericshop' );

	return $order_status;
}
add_filter( 'wc_order_statuses', 'add_order_status' );

/**
 * Add extra profile info at user profile
 *  ( Date of birth and Gender )
 *
 * @param array $user user.
 */
function add_extra_user_profile_fields( $user ) {
	$dob = esc_attr( get_the_author_meta( 'generic_billing_dob', $user->ID ) );
	$gender = esc_attr( get_the_author_meta( 'generic_billing_gender', $user->ID ) );

	$label = array(
		'costumer' => __( 'BACKEND_GENERAL_CUSTOMER', 'wc-genericshop' ),
		'dob' => __( 'BACKEND_GENERAL_DOB', 'wc-genericshop' ),
		'gender' => __( 'BACKEND_GENERAL_GENDER', 'wc-genericshop' ),
		'gender_male' => __( 'BACKEND_GENERAL_GENDER_MALE', 'wc-genericshop' ),
		'gender_female' => __( 'BACKEND_GENERAL_GENDER_FEMALE', 'wc-genericshop' ),
		'dob_placeholder' => __( 'BACKEND_GENERAL_DOB_PLACEHOLDER', 'wc-genericshop' ),
	);

	$args = array(
		'dob' => $dob,
		'gender' => $gender,
		'label' => $label,
	);
	// var_dump($args);
	// exit;
	genericshop_General_Functions::grs_include_template( dirname( __FILE__ ) . '/templates/admin/user/template-edit-user.php', $args );
}
if ( get_option( 'genericshop_general_dob_gender' ) ) {
	add_action( 'show_user_profile', 'add_extra_user_profile_fields' );
	add_action( 'edit_user_profile', 'add_extra_user_profile_fields' );
}

/**
 * Save extra profile info at user profile
 *  ( Date of birth and Gender )
 *
 * @param array $user_id user id.
 * @return bool
 */
function save_extra_user_profile_fields( $user_id ) {
	$saved = false;
	if ( current_user_can( 'edit_user', $user_id ) ) {
		$billing_dob = genericshop_General_Functions::grs_get_request_value( 'generic_billing_dob', '00-00-0000' );
		update_user_meta( $user_id, 'generic_billing_dob', $billing_dob );
		$billing_gender = genericshop_General_Functions::grs_get_request_value( 'generic_billing_gender', 'Male' );
		update_user_meta( $user_id, 'generic_billing_gender', $billing_gender );
		$saved = true;
	}
	return true;
}
if ( get_option( 'genericshop_general_dob_gender' ) ) {
	add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
	add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );
}

/**
 * Add date of birth & gender at user profile
 *
 * @param array $fields fields.
 * @return bool
 */
function custom_woocommerce_billing_fields( $fields ) {

	$label = array(
		'dob' => __( 'BACKEND_GENERAL_DOB', 'wc-genericshop' ),
		'gender' => __( 'BACKEND_GENERAL_GENDER', 'wc-genericshop' ),
		'dob_placeholder' => __( 'BACKEND_GENERAL_DOB_PLACEHOLDER', 'wc-genericshop' ),
	);

	$fields['generic_billing_dob'] = array(
		'type'          => 'text',
		'label'         => $label['dob'],
		'required'      => true,
		'placeholder'   => $label['dob_placeholder'],
		'class'         => array( 'form-row-first', 'address-field' ),
	);

	$fields['generic_billing_gender'] = array(
		'type'      => 'select',
		'label'     => $label['gender'],
		'options'   => array(
			'Male' => __( 'BACKEND_GENERAL_GENDER_MALE', 'wc-genericshop' ),
			'Female' => __( 'BACKEND_GENERAL_GENDER_FEMALE', 'wc-genericshop' ),
		),
		'required'  => true,
		'class'     => array( 'form-row-last', 'address-field' ),
		'clear'     => true,
	);

	return $fields;
}
if ( get_option( 'genericshop_general_dob_gender' ) ) {
	add_filter( 'woocommerce_billing_fields', 'custom_woocommerce_billing_fields' );
}

/**
 * Save date of birth and gender after ajax load payment method list
 *
 * @param  string $post_data post data.
 */
function action_checkout_update_order_review( $post_data ) {
	parse_str( $post_data, $output );
	WC()->session->set( 'generic_billing_dob', $output['generic_billing_dob'] );
	WC()->session->set( 'generic_billing_gender', $output['generic_billing_gender'] );
}
if ( get_option( 'genericshop_general_dob_gender' ) ) {
	add_action( 'woocommerce_checkout_update_order_review', 'action_checkout_update_order_review' );
}

/**
 * Add generic custom order status icon
 */
function generic_add_custom_order_status_icon() {
	if ( ! is_admin() ) {
		return;
	}
	?>
		<style>
			.column-order_status mark.pre-authorization:after, .column-order_status mark.payment-accepted:after, .column-order_status mark.in-review:after {
				background-size:100%;
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				text-align: center;
				content: '';
				background-repeat: no-repeat;
			}
			.column-order_status mark.pre-authorization:after {
				background-image: url(<?php echo esc_attr( plugins_url( 'assets/images/pre-authorization.png', __FILE__ ) ); ?>);
			}
			.column-order_status mark.payment-accepted:after {
				background-image: url(<?php echo esc_attr( plugins_url( 'assets/images/payment-accepted.png', __FILE__ ) ); ?>);
			}
			.column-order_status mark.in-review:after {
				background-image: url(<?php echo esc_attr( plugins_url( 'assets/images/in-review.png', __FILE__ ) ); ?>);
			}
		</style>
	<?php
}

add_action( 'wp_print_scripts', 'generic_add_custom_order_status_icon' );

/**
 * Change placeholder on address field in checkout page
 *
 * @param array $fields fields.
 * @return array
 */
function generic_label_wc_address_field( $fields ) {
	$label = array(
		'address_2_placeholder' => __( 'BACKEND_GENERAL_ADDRESS_2_PLACEHOLDER', 'wc-genericshop' ),
	);

	$fields['address_2']['placeholder'] = $label['address_2_placeholder'];

	return $fields;
}
add_filter( 'woocommerce_default_address_fields', 'generic_label_wc_address_field' );
