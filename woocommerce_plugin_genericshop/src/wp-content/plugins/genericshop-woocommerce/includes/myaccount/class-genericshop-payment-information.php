<?php
/**
 * Generic Shop Plugin Installation Process
 *
 * The class is used for My Payment Information
 * Copyright (c) Generic Shop
 *
 * @class       Genericshop_Payment_Information
 * @package     Genericshop/Classes
 * @located at  /includes/myaccount/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * The class is used for My Payment Information
 */
class Genericshop_Payment_Information extends WC_Shortcode_My_Account {

	/**
	 * Recurring
	 *
	 * @var boolean $recurring
	 */
	static $recurring;
	/**
	 * Home url
	 *
	 * @var string $home_url
	 */
	static $home_url;
	/**
	 * Current url
	 *
	 * @var string $current_url
	 */
	static $current_url;
	/**
	 * Clean current url
	 *
	 * @var string $clean_current_url
	 */
	static $clean_current_url;
	/**
	 * Plugin url
	 *
	 * @var string $plugin_url
	 */
	static $plugin_url;
	/**
	 * Plugin path
	 *
	 * @var string $plugin_path
	 */
	static $plugin_path;

	/**
	 * Main function of the class
	 */
	public static function init() {
		global $wp;

		self::$home_url = home_url();
		if ( isset( $wp->request ) ) {
			self::$current_url = home_url( $wp->request ) . '/?';
		} else {
			self::$current_url = get_page_link() . '&';
		}
		self::$clean_current_url = rtrim( self::$current_url, '?' );
		self::$plugin_url = plugins_url( '/', GENERIC_PLUGIN_FILE );
		self::$plugin_path = dirname( __FILE__ ) . '/../..';

		if ( ! is_user_logged_in() ) {
			wp_safe_redirect( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );
		} else {
			self::$recurring = get_option( 'genericshop_general_recurring' );
			if ( self::$recurring ) {
				$section = Genericshop_General_Functions::grs_get_request_value( 'section' );
				$page = isset( $wp->query_vars['page'] ) ? $wp->query_vars['page'] : '';
				switch ( $page ) {
					case 'wc-register':
						self::grs_register_payment( $section );
						break;
					case 'wc-reregister':
						self::grs_change_payment( $section );
						break;
					case 'wc-deregister':
						self::grs_delete_payment( $section );
						break;
					case 'wc-response-register':
						self::grs_response_register( $section, 'register' );
						break;
					case 'wc-response-reregister':
						self::grs_response_register( $section, 'reregister' );
						break;
					case 'wc-response-deregister':
						self::grs_response_deregister( $section );
						break;
					case 'wc-default':
						self::grs_set_default_payment( $section );
						break;
					default:
						self::grs_get_payment_information_page();
						break;
				}
			} else {
				wp_safe_redirect( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );
			}
		}// End if().
	}

	/**
	 * Load payment information page
	 * List of My Payment Information ( CC ).
	 */
	private static function grs_get_payment_information_page() {
		$cc_saved = new Gateway_Genericshop_CCSaved;

		$payment_gateway_recurring = array( $cc_saved );

		$args = array(
			'plugin_url' => self::$plugin_url,
			'recurring' => self::$recurring,
			'current_url' => self::$current_url,
		);
		foreach ( $payment_gateway_recurring as $payment_gateway ) {
			$payment_group = $payment_gateway->get_payment_group();
			$credentials = $payment_gateway->get_credentials();
			$registered_payments[ $payment_group ] = Genericshop_General_Models::grs_get_db_registered_payment( $payment_group, $credentials );

			$enabled = 'is_active_' . strtolower( $payment_group );
			$args[ $enabled ] = 'yes' === $payment_gateway->enabled ? true : false;
		}
		$args['registered_payments'] = $registered_payments;

		wp_enqueue_style( 'genericshop_formpayment_style', self::$plugin_url . '/assets/css/formpayment.css', array(), null );
		Genericshop_General_Functions::grs_include_template( self::$plugin_path . '/templates/myaccount/template-payment-information.php', $args );
	}

	/**
	 * Check if payment active
	 *
	 * @param string $payment_id payment id.
	 * @return bool
	 */
	private static function grs_is_active_payment( $payment_id ) {
		$payment_settings = get_option( 'woocommerce_' . $payment_id . '_settings' );

		if ( 'yes' === $payment_settings['enabled'] ) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * Set default payment account
	 *
	 * @param string $payment_id payment id.
	 */
	private static function grs_set_default_payment( $payment_id ) {
		$payment_gateway = Genericshop_General_Functions::grs_get_payment_gateway_by_id( $payment_id );
		$recurring_id = Genericshop_General_Functions::grs_get_request_value( 'id' );

		if ( $recurring_id ) {
			$payment_group = $payment_gateway->get_payment_group();
			$credentials = $payment_gateway->get_credentials();

			$query['field'] = 'payment_default';
			$query['value'] = '1';
			$query['payment_default'] = 0;
			Genericshop_General_Models::grs_update_db_default_payment( $query, $payment_group, $credentials );

			$query['field'] = 'id';
			$query['value'] = $recurring_id;
			$query['payment_default'] = 1;
			Genericshop_General_Models::grs_update_db_default_payment( $query, $payment_group, $credentials );
		}
		wp_safe_redirect( self::$clean_current_url );
	}

	/**
	 * Redirect error
	 *
	 * @param string $error_identifier error identifier.
	 * @param string $error_default error default.
	 */
	private static function grs_redirect_error( $error_identifier, $error_default = 'ERROR_UNKNOWN' ) {
		if ( 'ERROR_UNKNOWN' === $error_identifier ) {
			$error_identifier = $error_default;
		}
		$error_translated = Genericshop_General_Functions::grs_translate_error_identifier( $error_identifier );
		WC()->session->set( 'genericshop_myaccount_error', $error_translated );
		wp_safe_redirect( self::$clean_current_url );
		exit();
	}

	/**
	 * Load register payment account page
	 * Get chekout id & Payment widget from opp
	 *
	 * @param string $payment_id payment id.
	 */
	private static function grs_register_payment( $payment_id ) {
		$register_parameters = self::grs_get_register_parameters( $payment_id, true );
		$register_parameters['transaction_id'] = get_current_user_id();

		$checkout_result = GenericshopPaymentCore::get_checkout_result( $register_parameters );

		if ( ! $checkout_result['is_valid'] ) {
			self::grs_redirect_error( $checkout_result['response'] );
		} elseif ( ! isset( $checkout_result['response']['id'] ) ) {
			self::grs_redirect_error( 'ERROR_GENERAL_REDIRECT' );
		}

		$url_config['payment_widget'] = GenericshopPaymentCore::get_payment_widget_url( $register_parameters['server_mode'], $checkout_result['response']['id'] );
		$url_config['return_url'] = self::$current_url . 'page=wc-response-register&section=' . $payment_id;
		$url_config['cancel_url'] = self::$clean_current_url;

		$form_title = __( 'FRONTEND_MC_SAVE', 'wc-genericshop' );
		$confirm_button = __( 'FRONTEND_BT_REGISTER', 'wc-genericshop' );

		$payment_gateway = Genericshop_General_Functions::grs_get_payment_gateway_by_id( $payment_id );
		$args = array(
			'payment_id' => $payment_id,
			'plugin_url' => self::$plugin_url,
			'url_config' => $url_config,
			'form_title' => $form_title,
			'confirm_button' => $confirm_button,
			'register_parameters' => $register_parameters,
			'payment_brand' => $payment_gateway->get_payment_brand(),
		);
		wp_enqueue_script( 'genericshop_formpayment_script', $url_config['payment_widget'], array(), null );
		wp_enqueue_style( 'genericshop_formpayment_style', self::$plugin_url . '/assets/css/formpayment.css', array(), null );
		Genericshop_General_Functions::grs_include_template( self::$plugin_path . '/templates/myaccount/template-register.php', $args );
	}

	/**
	 * Load change ( reregisster ) payment account page
	 * Get chekout id & payment widget from opp
	 *
	 * @param string $payment_id payment id.
	 */
	private static function grs_change_payment( $payment_id ) {
		$id = Genericshop_General_Functions::grs_get_request_value( 'id' );

		$registration_id = Genericshop_General_Models::grs_get_db_registration_id( $id );
		$register_parameters = self::grs_get_register_parameters( $payment_id, true );
		$register_parameters['transaction_id'] = $registration_id;

		$checkout_result = GenericshopPaymentCore::get_checkout_result( $register_parameters );

		if ( ! $checkout_result['is_valid'] ) {
			self::grs_redirect_error( $checkout_result['response'] );
		} elseif ( ! isset( $checkout_result['response']['id'] ) ) {
			self::grs_redirect_error( 'ERROR_GENERAL_REDIRECT' );
		}

		$url_config['payment_widget'] = GenericshopPaymentCore::get_payment_widget_url( $register_parameters['server_mode'], $checkout_result['response']['id'] );
		$url_config['return_url'] = self::$current_url . 'page=wc-response-reregister&section=' . $payment_id . '&recurring_id=' . $id;
		$url_config['cancel_url'] = self::$clean_current_url;

		$form_title = __( 'FRONTEND_MC_CHANGE', 'wc-genericshop' );
		$confirm_button = __( 'FRONTEND_BT_CHANGE', 'wc-genericshop' );

		$payment_gateway = Genericshop_General_Functions::grs_get_payment_gateway_by_id( $payment_id );
		$args = array(
			'payment_id' => $payment_id,
			'plugin_url' => self::$plugin_url,
			'url_config' => $url_config,
			'form_title' => $form_title,
			'confirm_button' => $confirm_button,
			'register_parameters' => $register_parameters,
			'payment_brand' => $payment_gateway->get_payment_brand(),
		);
		wp_enqueue_script( 'genericshop_formpayment_script', $url_config['payment_widget'], array(), null );
		wp_enqueue_style( 'genericshop_formpayment_style', self::$plugin_url . '/assets/css/formpayment.css', array(), null );
		Genericshop_General_Functions::grs_include_template( self::$plugin_path . '/templates/myaccount/template-register.php', $args );
	}

	/**
	 * Load delete ( deregisster ) payment account page
	 *
	 * @param string $payment_id payment id.
	 */
	private static function grs_delete_payment( $payment_id ) {
		$id = Genericshop_General_Functions::grs_get_request_value( 'id' );
		$url_config['return_url'] = self::$current_url . 'page=wc-response-deregister&section=' . $payment_id . '&recurring_id=' . $id;
		$url_config['cancel_url'] = self::$clean_current_url;

		$args = array(
			'plugin_url' => self::$plugin_url,
			'url_config' => $url_config,
		);

		wp_enqueue_style( 'genericshop_formpayment_style', self::$plugin_url . '/assets/css/formpayment.css', array(), null );
		Genericshop_General_Functions::grs_include_template( self::$plugin_path . '/templates/myaccount/template-deregister.php', $args );
	}

	/**
	 * Response from register/reregister payment account
	 * Get status & response from opp
	 *
	 * @param string $payment_id payment id.
	 * @param string $register_method register method.
	 */
	private static function grs_response_register( $payment_id, $register_method ) {
		$payment_gateway = Genericshop_General_Functions::grs_get_payment_gateway_by_id( $payment_id );
		$checkout_id = Genericshop_General_Functions::grs_get_request_value( 'id' );
		$recurring_id = Genericshop_General_Functions::grs_get_request_value( 'recurring_id' );

		$is_testmode_available = false;
		$credentials = $payment_gateway->get_credentials( $is_testmode_available );
		$register_result = GenericshopPaymentCore::get_payment_status( $checkout_id, $credentials );

		if ( ! $register_result['is_valid'] ) {
			self::grs_redirect_error( $register_result['response'], 'ERROR_GENERAL_NORESPONSE' );
		} else {
			$register_status = GenericshopPaymentCore::get_transaction_result( $register_result['response']['result']['code'] );

			Genericshop_General_Functions::add_log( 'Register payment result ', $register_result );

			if ( 'ACK' === $register_status ) {
				self::grs_do_success_register( $payment_id, $register_result['response'], $recurring_id );
				if ( 'register' === $register_method ) {

					Genericshop_General_Functions::add_log( 'Register payment is success' );

					$success_message = __( 'SUCCESS_MC_ADD', 'wc-genericshop' );
				} else {

					Genericshop_General_Functions::add_log( 'Change payment is success' );

					$success_message = __( 'SUCCESS_MC_UPDATE', 'wc-genericshop' );
				}

				WC()->session->set( 'genericshop_myaccount_success', $success_message );
			} else {
				if ( 'register' === $register_method ) {

					Genericshop_General_Functions::add_log( 'Register payment is failed' );

					$error_message = __( 'ERROR_MC_ADD', 'wc-genericshop' );
				} else {

					Genericshop_General_Functions::add_log( 'Change payment is failed' );

					$error_message = __( 'ERROR_MC_UPDATE', 'wc-genericshop' );
				}

				WC()->session->set( 'genericshop_myaccount_error', $error_message );
			}
			wp_safe_redirect( self::$clean_current_url );
		}// End if().
	}

	/**
	 * Success register
	 * Action if register is success
	 *
	 * @param string $payment_id payment id.
	 * @param array  $register_result payment id.
	 * @param string $recurring_id recurring_id.
	 */
	private static function grs_do_success_register( $payment_id, $register_result, $recurring_id ) {
		$payment_gateway = Genericshop_General_Functions::grs_get_payment_gateway_by_id( $payment_id );
		$deregister_id = $register_result['merchantTransactionId'];
		$reference_id = $register_result['id'];

		$register_parameters = self::grs_get_register_parameters( $payment_id );
		if ( $recurring_id ) {
			$register_parameters['transaction_id'] = $register_result['merchantTransactionId'];
		} else {
			$register_parameters['transaction_id'] = get_current_user_id();
		}

		$registration_id = $register_result['registrationId'];

		self::grs_refund_registered_payment( $registration_id, $reference_id, $register_parameters, $recurring_id );

		if ( $recurring_id ) {
			GenericshopPaymentCore::delete_registered_account( $deregister_id, $register_parameters );
		}

		self::grs_save_registered_payment( $payment_id, $register_result, $recurring_id );
	}

	/**
	 * Response from deregister payment
	 * Deregister & get response from opp
	 *
	 * @param string $payment_id payment id.
	 */
	private static function grs_response_deregister( $payment_id ) {
		$payment_gateway = Genericshop_General_Functions::grs_get_payment_gateway_by_id( $payment_id );
		$recurring_id = Genericshop_General_Functions::grs_get_request_value( 'recurring_id', 0 );
		$registration_id = Genericshop_General_Models::grs_get_db_registration_id( $recurring_id );

		$register_parameters = $payment_gateway->get_credentials();
		$register_parameters['transaction_id'] = get_current_user_id();

		$deregister_result = GenericshopPaymentCore::delete_registered_account( $registration_id, $register_parameters );

		Genericshop_General_Functions::add_log( 'Deregister payment result ', $deregister_result );

		if ( ! $deregister_result['is_valid'] ) {
			Genericshop_General_Functions::add_log( 'Deregister payment is failed : ' . $deregister_result['response'] );
			self::grs_redirect_error( $deregister_result['response'], 'ERROR_MC_DELETE' );
		} else {
			$deregister_status = GenericshopPaymentCore::get_transaction_result( $deregister_result['response']['result']['code'] );
			if ( 'ACK' === $deregister_status ) {

				Genericshop_General_Functions::add_log( 'Deregister payment is success' );

				Genericshop_General_Models::grs_delete_db_registered_payment( $recurring_id );
				WC()->session->set( 'genericshop_myaccount_success', __( 'SUCCESS_MC_DELETE', 'wc-genericshop' ) );
			} else {

				Genericshop_General_Functions::add_log( 'Deregister payment is failed' );

				WC()->session->set( 'genericshop_myaccount_error', __( 'ERROR_MC_DELETE', 'wc-genericshop' ) );
			}
		}

		wp_safe_redirect( self::$clean_current_url );
	}

	/**
	 * Delete registered account
	 *
	 * @param string $payment_id payment id.
	 * @param int    $registration_id registration id.
	 */
	private static function grs_delete_registered_account( $payment_id, $registration_id ) {
		$payment_gateway = Genericshop_General_Functions::grs_get_payment_gateway_by_id( $payment_id );
		Genericshop_General_Models::grs_delete_db_registered_payment_by_regid( $registration_id );
		$register_parameters = $payment_gateway->get_credentials();
		GenericshopPaymentCore::delete_registered_account( $registration_id, $register_parameters );
	}

	/**
	 * Refund after success register payment account
	 * Refund & Capture/Reversal Amount from success register payment account
	 *
	 * @param int   $registration_id registration id.
	 * @param int   $reference_id reference id.
	 * @param array $register_parameters register parameters.
	 * @param array $recurring_id recurring id.
	 */
	private static function grs_refund_registered_payment( $registration_id, $reference_id, $register_parameters, $recurring_id ) {
		$payment_type = $register_parameters['payment_type'];
		$register_parameters['payment_type'] = ( 'DB' === $payment_type )? 'RF' : 'CP';
		$backoffice_result = GenericshopPaymentCore::back_office_operation( $reference_id, $register_parameters );

		if ( 'PA' === $payment_type ) {
			if ( $recurring_id ) {
				$error_default = 'ERROR_MC_UPDATE';
			} else {
				$error_default = 'ERROR_MC_ADD';
			}
			if ( ! $backoffice_result['is_valid'] ) {
				GenericshopPaymentCore::delete_registered_account( $registration_id, $register_parameters );
				self::grs_redirect_error( $backoffice_result['response'], $error_default );
			} else {
				$transaction_result = GenericshopPaymentCore::get_transaction_result( $backoffice_result['response']['result']['code'] );
				if ( 'ACK' === $transaction_result ) {
					$reference_id = $backoffice_result['response']['id'];
					$register_parameters['payment_type'] = 'RF';
					GenericshopPaymentCore::back_office_operation( $reference_id, $register_parameters );
				} else {
					GenericshopPaymentCore::delete_registered_account( $registration_id, $register_parameters );
					self::grs_redirect_error( $error_default );
				}
			}
		}
	}

	/**
	 * Get parameter for register payment account
	 *
	 * @param string $payment_id payment id.
	 * @param bool   $registration recuring or not.
	 * @return $register_parameters
	 */
	private static function grs_get_register_parameters( $payment_id, $registration = false ) {
		$payment_gateway = Genericshop_General_Functions::grs_get_payment_gateway_by_id( $payment_id );
		$payment_settings = get_option( 'woocommerce_' . $payment_id . '_settings' );
		$register_parameters = $payment_gateway->get_credentials();

		$register_parameters['amount']         = $payment_settings['amount_registration'];
		$register_parameters['currency']       = get_woocommerce_currency();
		$register_parameters['payment_type']   = $payment_gateway->get_payment_type();

		if ( $registration ) {
			$register_parameters['payment_recurring'] = 'INITIAL';
			$register_parameters['payment_registration'] = 'true';
		}

		if ( 'genericshop_ccsaved' === $payment_id ) {
			$register_parameters['3D']['amount']    = $register_parameters['amount'] ;
			$register_parameters['3D']['currency']  = $register_parameters['currency'];
		}

		$register_parameters['customer']['first_name']   = esc_attr( get_the_author_meta( 'billing_first_name', get_current_user_id() ) );
		$register_parameters['customer']['last_name']     = esc_attr( get_the_author_meta( 'billing_last_name', get_current_user_id() ) );
		$register_parameters['customer']['phone']         = esc_attr( get_the_author_meta( 'billing_phone', get_current_user_id() ) );
		$register_parameters['customer']['email']         = esc_attr( get_the_author_meta( 'billing_email', get_current_user_id() ) );
		$register_parameters['billing']['street']         = esc_attr( get_the_author_meta( 'billing_address_1', get_current_user_id() ) );
		$register_parameters['billing']['city']         = esc_attr( get_the_author_meta( 'billing_city', get_current_user_id() ) );
		$register_parameters['billing']['zip']           = esc_attr( get_the_author_meta( 'billing_postcode', get_current_user_id() ) );
		$register_parameters['billing']['country_code'] = esc_attr( get_the_author_meta( 'billing_country', get_current_user_id() ) );

		$register_parameters['customer_ip'] = Genericshop_General_Functions::get_customer_ip();

		return $register_parameters;
	}

	/**
	 * Save registered payment account to db
	 *
	 * @param string $payment_id payment id.
	 * @param array  $register_result register result.
	 * @param int    $recurring_id recurring id.
	 */
	private static function grs_save_registered_payment( $payment_id, $register_result, $recurring_id ) {
		global $wpdb;

		$payment_gateway = Genericshop_General_Functions::grs_get_payment_gateway_by_id( $payment_id );
		$payment_group = $payment_gateway->get_payment_group();
		$credentials = $payment_gateway->get_credentials();
		$registered_payment = Genericshop_General_Functions::grs_get_account_by_result( $payment_id, $register_result );

		$registered_payment['payment_brand'] = $register_result['paymentBrand'];
		$registered_payment['payment_group'] = $payment_group;
		$registered_payment['payment_default'] = Genericshop_General_Models::grs_get_db_payment_default( $payment_group, $credentials );
		$registered_payment['server_mode'] = $credentials['server_mode'];
		$registered_payment['channel_id'] = $credentials['channel_id'];
		$registered_payment['registration_id'] = $register_result['registrationId'];

		if ( $recurring_id ) {
			Genericshop_General_Models::grs_update_db_registered_payment( $recurring_id, $registered_payment );
		} else {
			Genericshop_General_Models::grs_save_db_registered_payment( $registered_payment );
		}
	}

}

add_shortcode( 'woocommerce_my_payment_information', 'Genericshop_Payment_Information::init' );
