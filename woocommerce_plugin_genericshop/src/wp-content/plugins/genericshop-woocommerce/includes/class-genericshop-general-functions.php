<?php
/**
 * Genericshop General Functions
 *
 * General functions available on both the front-end and admin.
 * Copyright (c) Generic Shop
 *
 * @class       Genericshop_General_Functions
 * @package     Genericshop/Classes
 * @located at  /includes/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * General functions available on both the front-end and admin.
 */
class Genericshop_General_Functions {
	/**
	 * Percent for discount or  tax
	 *
	 * @var int $percent percent.
	 */
	public static $percent = 100;

	/**
	 * Get the plugin url.
	 *
	 * @return string
	 */
	public static function grs_get_plugin_url() {

		return untrailingslashit( plugins_url( '/', GENERIC_PLUGIN_FILE ) );
	}

	/**
	 * Return the WordPress language
	 *
	 * @return string
	 */
	public static function grs_get_shop_language() {

		return ( substr( get_bloginfo( 'language' ), 0, 2 ) === 'de' ) ? 'de' : 'en' ;
	}

	/**
	 * Get payment gateway instance by id
	 *
	 * @param string $id id.
	 * @return object
	 */
	public static function grs_get_payment_gateway_by_id( $id ) {
		$method = ucfirst( str_replace( 'genericshop_', '', $id ) );
		$method = str_replace( 'Cc', 'CC', $method );
		$method = str_replace( 'saved', 'Saved', $method );

		$class_name = 'Gateway_Genericshop_' . $method;
		$payment_gateway = new $class_name();

		return $payment_gateway;
	}

	/**
	 * Get Customer IP.
	 *
	 * @return string
	 */
	public static function get_customer_ip() {
		$server_remote_addr = '';
		$server = $_SERVER; // input var okay.
		if ( isset( $server['REMOTE_ADDR'] ) ) {
			$server_remote_addr = sanitize_text_field( wp_unslash( $server['REMOTE_ADDR'] ) );
		}

		if ( '::1' === $server_remote_addr ) {
			$customer_ip = '127.0.0.1';
		} else {
			$customer_ip = $server_remote_addr;
		}

		return $customer_ip;
	}

	/**
	 * Check grs is version tracker active
	 *
	 * @return bool
	 */
	public static function grs_is_version_tracker_active() {

		return true;
	}

	/**
	 * Get Recurring Payment
	 *
	 * @return array
	 */
	public static function grs_get_recurring_payment() {
		$payments = array( 'genericshop_ccsaved' );

		return $payments;
	}

	/**
	 * Get Payment Form
	 *
	 * @param string $payment_id payment id.
	 * @return string
	 */
	public static function grs_get_payment_form( $payment_id ) {
		return $payment_form = 'form';
	}

	/**
	 * Get Payment Id By Group
	 *
	 * @param string $payment_group payment group.
	 * @return string
	 */
	public static function grs_get_payment_id_by_group( $payment_group ) {
		return $payment_id = 'genericshop_ccsaved';
	}

	/**
	 * Is recuring
	 *
	 * @param string $payment_id payment id.
	 * @return bool
	 */
	public static function is_recurring( $payment_id ) {
		switch ( $payment_id ) {
			case 'genericshop_ccsaved':
				return true;
			default:
				return false;
		}
	}

	/**
	 * Get Account Card Info from gateway
	 *
	 * @param string $payment_id payment id.
	 * @param array  $payment_result payment result.
	 * @return array
	 */
	public static function grs_get_account_by_result( $payment_id, $payment_result ) {
		$account_card['holder']         = $payment_result['card']['holder'];
		$account_card['last_4_digits']  = $payment_result['card']['last4Digits'];
		$account_card['expiry_month']   = $payment_result['card']['expiryMonth'];
		$account_card['expiry_year']    = $payment_result['card']['expiryYear'];
		$account_card['email']          = '';
		return $account_card;

	}

	/**
	 * Get Payment Price with Tax and Discount.
	 *
	 * @param array $cart cart.
	 * @return float
	 */
	public static function grs_get_payment_price_with_tax_and_discount( $cart ) {
		global $woocommerce;
		$wc_cart = $woocommerce->cart;

		$is_prices_include_tax = get_option( 'woocommerce_prices_include_tax' );
		$price = $cart['data']->get_price();
		$price_with_discount = $price;

		if ( 'no' === $is_prices_include_tax ) {
			$tax = $cart['line_tax'] / $cart['quantity'];
			$price_with_discount_and_tax = $price + $tax;
		} else {
			$price_with_discount_and_tax = $price;
		}

		return GenericshopPaymentCore::set_number_format( $price_with_discount_and_tax );
	}

	/**
	 * Get Payment Discount from Cart Order.
	 *
	 * @param array $cart cart.
	 * @return float
	 */
	public static function grs_get_payment_discount_in_percent( $cart ) {
		$product_detail = Genericshop_General_Models::grs_get_db_product_detail( $cart['data']->get_id() );

		$regular_price = $product_detail['_regular_price'];
		$sale_price = $cart['data']->get_price();

		if ( $regular_price !== $sale_price ) {
			$discount = $regular_price - $sale_price;
			$discount_percent = ( $discount / $regular_price ) * self::$percent;

			return GenericshopPaymentCore::set_number_format( $discount_percent );
		}

		return false;
	}

	/**
	 * Get Payment Tax from Cart Order.
	 *
	 * @param array $cart cart.
	 * @return float
	 */
	public static function grs_get_payment_tax_in_percent( $cart ) {
		$is_enable_tax = get_option( 'woocommerce_calc_taxes' );
		$product_detail = Genericshop_General_Models::grs_get_db_product_detail( $cart['data']->get_id() );
		if ( isset( $product_detail['_tax_status'] ) ) {
			$is_enable_tax_product = $product_detail['_tax_status'];
		}
		if ( isset( $is_enable_tax_product ) && 'yes' === $is_enable_tax && 'taxable' === $is_enable_tax_product ) {
			$tax_precent = ( $cart['line_tax'] / $cart['line_total'] ) * self::$percent;
			return GenericshopPaymentCore::set_number_format( $tax_precent );
		}

		return false;
	}

	/**
	 * Get Payment Tax from Cart Order.
	 *
	 * @param string $gender gender.
	 * @return string
	 */
	public static function grs_get_initial_gender( $gender ) {
		switch ( $gender ) {
			case 'Male':
				$initial_gender = 'M';
				break;
			case 'Female':
				$initial_gender = 'F';
				break;
			default:
				$initial_gender = '';
				break;
		}

		return $initial_gender;
	}

	/**
	 * Translate Error Identifier.
	 *
	 * @param string $error_identifier error identifier.
	 * @return string
	 */
	public static function grs_translate_error_identifier( $error_identifier ) {
		switch ( $error_identifier ) {
			case 'ERROR_CC_ACCOUNT':
				$error_translate = __( 'ERROR_CC_ACCOUNT', 'wc-genericshop' );
				break;
			case 'ERROR_CC_INVALIDDATA':
				$error_translate = __( 'ERROR_CC_INVALIDDATA', 'wc-genericshop' );
				break;
			case 'ERROR_CC_BLACKLIST':
				$error_translate = __( 'ERROR_CC_BLACKLIST', 'wc-genericshop' );
				break;
			case 'ERROR_CC_DECLINED_CARD':
				$error_translate = __( 'ERROR_CC_DECLINED_CARD', 'wc-genericshop' );
				break;
			case 'ERROR_CC_EXPIRED':
				$error_translate = __( 'ERROR_CC_EXPIRED', 'wc-genericshop' );
				break;
			case 'ERROR_CC_INVALIDCVV':
				$error_translate = __( 'ERROR_CC_INVALIDCVV', 'wc-genericshop' );
				break;
			case 'ERROR_CC_EXPIRY':
				$error_translate = __( 'ERROR_CC_EXPIRY', 'wc-genericshop' );
				break;
			case 'ERROR_CC_LIMIT_EXCEED':
				$error_translate = __( 'ERROR_CC_LIMIT_EXCEED', 'wc-genericshop' );
				break;
			case 'ERROR_CC_3DAUTH':
				$error_translate = __( 'ERROR_CC_3DAUTH', 'wc-genericshop' );
				break;
			case 'ERROR_CC_3DERROR':
				$error_translate = __( 'ERROR_CC_3DERROR', 'wc-genericshop' );
				break;
			case 'ERROR_CC_NOBRAND':
				$error_translate = __( 'ERROR_CC_NOBRAND', 'wc-genericshop' );
				break;
			case 'ERROR_GENERAL_LIMIT_AMOUNT':
				$error_translate = __( 'ERROR_GENERAL_LIMIT_AMOUNT', 'wc-genericshop' );
				break;
			case 'ERROR_GENERAL_LIMIT_TRANSACTIONS':
				$error_translate = __( 'ERROR_GENERAL_LIMIT_TRANSACTIONS', 'wc-genericshop' );
				break;
			case 'ERROR_CC_DECLINED_AUTH':
				$error_translate = __( 'ERROR_CC_DECLINED_AUTH', 'wc-genericshop' );
				break;
			case 'ERROR_GENERAL_DECLINED_RISK':
				$error_translate = __( 'ERROR_GENERAL_DECLINED_RISK', 'wc-genericshop' );
				break;
			case 'ERROR_CC_ADDRESS':
				$error_translate = __( 'ERROR_CC_ADDRESS', 'wc-genericshop' );
				break;
			case 'ERROR_GENERAL_CANCEL':
				$error_translate = __( 'ERROR_GENERAL_CANCEL', 'wc-genericshop' );
				break;
			case 'ERROR_CC_RECURRING':
				$error_translate = __( 'ERROR_CC_RECURRING', 'wc-genericshop' );
				break;
			case 'ERROR_CC_REPEATED':
				$error_translate = __( 'ERROR_CC_REPEATED', 'wc-genericshop' );
				break;
			case 'ERROR_GENERAL_ADDRESS':
				$error_translate = __( 'ERROR_GENERAL_ADDRESS', 'wc-genericshop' );
				break;
			case 'ERROR_GENERAL_BLACKLIST':
				$error_translate = __( 'ERROR_GENERAL_BLACKLIST', 'wc-genericshop' );
				break;
			case 'ERROR_GENERAL_GENERAL':
				$error_translate = __( 'ERROR_GENERAL_GENERAL', 'wc-genericshop' );
				break;
			case 'ERROR_GENERAL_TIMEOUT':
				$error_translate = __( 'ERROR_GENERAL_TIMEOUT', 'wc-genericshop' );
				break;
			case 'ERROR_GIRO_NOSUPPORT':
				$error_translate = __( 'ERROR_GIRO_NOSUPPORT', 'wc-genericshop' );
				break;
			case 'ERROR_CAPTURE_BACKEND':
				$error_translate = __( 'ERROR_CAPTURE_BACKEND', 'wc-genericshop' );
				break;
			case 'ERROR_REORDER_BACKEND':
				$error_translate = __( 'ERROR_REORDER_BACKEND', 'wc-genericshop' );
				break;
			case 'ERROR_REFUND_BACKEND':
				$error_translate = __( 'ERROR_REFUND_BACKEND', 'wc-genericshop' );
				break;
			case 'ERROR_RECEIPT_BACKEND':
				$error_translate = __( 'ERROR_RECEIPT_BACKEND', 'wc-genericshop' );
				break;
			case 'ERROR_ADDRESS_PHONE':
				$error_translate = __( 'ERROR_ADDRESS_PHONE', 'wc-genericshop' );
				break;
			case 'ERROR_CAPTURE_BACKEND':
				$error_translate = __( 'ERROR_CAPTURE_BACKEND', 'wc-genericshop' );
				break;
			case 'ERROR_REORDER_BACKEND':
				$error_translate = __( 'ERROR_REORDER_BACKEND', 'wc-genericshop' );
				break;
			case 'ERROR_REFUND_BACKEND':
				$error_translate = __( 'ERROR_REFUND_BACKEND', 'wc-genericshop' );
				break;
			case 'ERROR_RECEIPT_BACKEND':
				$error_translate = __( 'ERROR_RECEIPT_BACKEND', 'wc-genericshop' );
				break;
			case 'ERROR_GENERAL_NORESPONSE':
				$error_translate = __( 'ERROR_GENERAL_NORESPONSE', 'wc-genericshop' );
				break;
			case 'ERROR_WRONG_DOB':
				$error_translate = __( 'ERROR_WRONG_DOB', 'wc-genericshop' );
				break;
			case 'ERROR_PARAMETER_GENDER':
				$error_translate = __( 'ERROR_PARAMETER_GENDER', 'wc-genericshop' );
				break;
			case 'ERROR_GENERAL_REDIRECT':
				$error_translate = __( 'ERROR_GENERAL_REDIRECT', 'wc-genericshop' );
				break;
			case 'ERROR_MC_ADD':
				$error_translate = __( 'ERROR_MC_ADD', 'wc-genericshop' );
				break;
			case 'ERROR_MC_UPDATE':
				$error_translate = __( 'ERROR_MC_UPDATE', 'wc-genericshop' );
				break;
			case 'ERROR_MC_DELETE':
				$error_translate = __( 'ERROR_MC_DELETE', 'wc-genericshop' );
				break;
			case 'ERROR_MERCHANT_SSL_CERTIFICATE':
				$error_translate = __( 'ERROR_MERCHANT_SSL_CERTIFICATE', 'wc-genericshop' );
				break;
			default:
				$error_translate = __( 'ERROR_UNKNOWN', 'wc-genericshop' );
				break;
		}// End switch().

		return $error_translate;
	}

	/**
	 * Translate genericshop_term
	 *
	 * @param string $genericshop_term term.
	 * @return string
	 */
	public static function grs_translate_genericshop_term( $genericshop_term ) {
		switch ( $genericshop_term ) {
			case 'GENERICSHOP_TT_VERSIONTRACKER':
				$genericshop_term_translate = __( 'GENERICSHOP_TT_VERSIONTRACKER', 'wc-genericshop' );
				break;
			case 'GENERICSHOP_BACKEND_BT_ADMIN':
				$genericshop_term_translate = __( 'GENERICSHOP_BACKEND_BT_ADMIN', 'wc-genericshop' );
				break;
		}
		return $genericshop_term_translate;
	}

	/**
	 * Get _REQUEST value
	 *
	 * @param string $key key.
	 * @param string $default default.
	 * @return value
	 */
	public static function grs_get_request_value( $key, $default = false ) {
		if ( isset( $_REQUEST[ $key ] ) ) {// input var okay.
			return sanitize_text_field( wp_unslash( $_REQUEST[ $key ] ) ); // input var okay.
		}
		return $default;
	}

	/**
	 * Include template
	 *
	 * @param string $template_file_path template file path (templates/template.php).
	 * @param array  $args variable to include in template.
	 */
	public static function grs_include_template( $template_file_path, $args = array() ) {
		if ( function_exists( 'wc_get_template' ) ) {
			$template = pathinfo( $template_file_path );
			$template_path = $template['dirname'] . '/';
			$template_file = $template['basename'];
			wc_get_template( $template_file,
				$args,
				$template_path,
				$template_path
			);
		} else {
			foreach ( $args as $key => $value ) {
				$$key = $value;
			}
			include( $template_file_path );
		}
	}

	/**
	 * Validate birth of date
	 *
	 * @param  string $dob birth of date.
	 * @return string|boolean
	 */
	public static function grs_is_date_of_birth_valid( $dob ) {
		if ( ! empty( $dob ) ) {

			$customer_date_of_birth = explode( '.', $dob );

			if ( ! isset( $customer_date_of_birth[0] ) ) {
				return false;
			}
			if ( ! isset( $customer_date_of_birth[1] ) ) {
				return false;
			}
			if ( ! isset( $customer_date_of_birth[2] ) ) {
				return false;
			}

			$day = (int) $customer_date_of_birth[0];
			$month = (int) $customer_date_of_birth[1];
			$year = (int) $customer_date_of_birth[2];

			if ( $year < 1900 ) {
				return false;
			}

			if ( $month < 1 || $month > 12 ) {
				return false;
			}

			if ( $day < 1 || $day > 31 ) {
				return false;
			}

			$valid = checkdate( $month, $day, $year );

			if ( $valid ) {
				return true;
			}
		}// End if().
		return false;
	}

	/**
	 * Validate future birth of date
	 *
	 * @param  string $dob birth of date.
	 * @return string|boolean
	 */
	public static function grs_is_date_of_birth_lower_than_today( $dob ) {
		$customer_date_of_birth = strtotime( $dob );
		$today = strtotime( date( 'd-m-Y' ) );

		if ( $customer_date_of_birth >= $today ) {
			return false;
		}
		return true;
	}

	/**
	 * Is woocommerce version greater than
	 *
	 * @param string $version woocommerce version.
	 * @return boolean
	 */
	public static function is_version_greater_than( $version ) {
		if ( class_exists( 'WooCommerce' ) ) {
			global $woocommerce;
			if ( version_compare( $woocommerce->version, $version, '>=' ) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get wc customer property value
	 *
	 * @param  string $property property of class WC_Order.
	 * @return mixed
	 */
	protected static function get_wc_customer_property_value( $property ) {
		global $woocommerce;

		$customer = $woocommerce->customer;
		$function = $property;
		return $customer->$function();
	}

	/**
	 * Check if billing address and shipping addres are equal
	 *
	 * @return boolean
	 */
	public static function grs_is_address_billing_equal_shipping() {
		global $woocommerce;

		$customer = $woocommerce->customer;

		if ( self::is_version_greater_than( '3.0' ) ) {
			$billing['street']        = self::get_wc_customer_property_value( 'get_billing_address_1' ) . ', ' . self::get_wc_customer_property_value( 'get_billing_address_2' );
			$billing['city']          = self::get_wc_customer_property_value( 'get_billing_city' );
			$billing['zip']           = self::get_wc_customer_property_value( 'get_billing_postcode' );
			$billing['country_code']  = self::get_wc_customer_property_value( 'get_billing_country' );
			$shipping['street']        = self::get_wc_customer_property_value( 'get_shipping_address_1' ) . ', ' . self::get_wc_customer_property_value( 'get_shipping_address_2' );
			$shipping['city']          = self::get_wc_customer_property_value( 'get_shipping_city' );
			$shipping['zip']           = self::get_wc_customer_property_value( 'get_shipping_postcode' );
			$shipping['country_code']  = self::get_wc_customer_property_value( 'get_shipping_country' );
		} else {
			$billing['street']        = self::get_wc_customer_property_value( 'get_address' ) . ', ' . self::get_wc_customer_property_value( 'get_address_2' );
			$billing['city']          = self::get_wc_customer_property_value( 'get_city' );
			$billing['zip']           = self::get_wc_customer_property_value( 'get_postcode' );
			$billing['country_code']  = self::get_wc_customer_property_value( 'get_country' );
			$shipping['street']        = self::get_wc_customer_property_value( 'get_shipping_address' ) . ', ' . self::get_wc_customer_property_value( 'get_shipping_address_2' );
			$shipping['city']          = self::get_wc_customer_property_value( 'get_shipping_city' );
			$shipping['zip']           = self::get_wc_customer_property_value( 'get_shipping_postcode' );
			$shipping['country_code']  = self::get_wc_customer_property_value( 'get_shipping_country' );
		}

		foreach ( $billing as $i => $bill ) {
			if ( $bill !== $shipping[ $i ] ) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check if shipping method is chosen
	 *
	 * @return boolean
	 */
	public static function grs_is_shipping_method_chosen() {
		$chosen_shipping_method = WC()->session->get( 'chosen_shipping_methods' );

		if ( ! is_null( $chosen_shipping_method[0] ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Determines if amount allowed
	 *
	 * @param int $total_amount total amount.
	 * @return boolean
	 */
	public static function grs_is_amount_allowed( $total_amount ) {

		$is_currency_euro = self::grs_is_currency_euro();

		if ( $is_currency_euro || $total_amount < 200 || $total_amount > 5000 ) {
			return false;
		}
		return true;
	}

	/**
	 * Determines if currency euro.
	 *
	 * @return boolean
	 */
	public static function grs_is_currency_euro() {
		$currency = get_woocommerce_currency();

		if ( 'EUR' !== $currency ) {
			return true;
		}
		return false;
	}

	/**
	 * Get customer order count
	 *
	 * @return int count
	 */
	public static function grs_get_order_count() {
		if ( wc_get_customer_order_count( get_current_user_id() ) > 0 ) {
			return wc_get_customer_order_count( get_current_user_id() );
		}
		return 0;
	}

	/**
	 * Get customer created date
	 *
	 * @return string|boolean created date
	 */
	public static function grs_get_customer_created_date() {
		if ( is_user_logged_in() ) {
			$user_data = get_userdata( get_current_user_id() );
			$created_date = strtotime( $user_data->user_registered );

			if ( isset( $user_data->user_registered ) && $created_date > 0 ) {
				return date( 'Y-m-d', $created_date );
			}
		}
		return date( 'Y-m-d' );
	}

	/**
	 * Add log for debugging
	 *
	 * @param string            $message - message.
	 * @param bool|string|array $data - data.
	 * @param bool|int          $order_id - order id.
	 */
	public static function add_log( $message, $data = false, $order_id = false ) {
		$genericshop_log = new WC_Logger();
		$logger_handle = 'genericshop-' . date( 'Ym' );

		if ( ! $order_id ) {
			$logger_message = $message;
		} elseif ( isset( $order_id ) || '0' !== $order_id ) {
			$wc_order = new WC_Order( $order_id );
			$order_id = $wc_order->get_order_number();

			$logger_message = '[Order Id : ' . $order_id . '] ' . $message;
		}

		if ( $data ) {
			if ( is_object( $data ) ) {
				$logger_message .= " : Xml ( \r\n";
				foreach ( $data->children() as $child ) {
					$logger_message .= '(' . $child->getName() . ') => ' . $child . "\r\n";
				}
				$logger_message .= ')';
			} elseif ( is_array( $data ) ) {
				$logger_message .= print_r( $data , 1 );
			} else {
				$logger_message .= ' : ' . $data;
			}
		}

		$genericshop_log->add( $logger_handle, $logger_message . "\r\n" );
	}
}
