<?php
/**
 * Generic Shop Credit Card
 *
 * The gateway is used for Credit card.
 * Copyright (c) Generic Shop
 *
 * @class      Gateway_Genericshop_CC
 * @package    Genericshop/Gateway
 * @extends    Genericshop_Payment_Gateway
 * @located at /includes/gateways
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * The gateway is used for Credit card.
 */
class Gateway_Genericshop_CC extends Genericshop_Payment_Gateway {
	/**
	 * Identifier CC
	 *
	 * @var string $id
	 */
	public $id = 'genericshop_cc';

	/**
	 * Payment brand
	 *
	 * @var string $payment_brand
	 */
	protected $payment_brand = 'VISA MASTER AMEX DINERS JCB';

	/**
	 * From class WC_Payment_Gateway
	 * Payment gateway icon.
	 */
	public function get_icon() {
		$icon_html = $this->grs_get_multi_icon();
		return apply_filters( 'woocommerce_gateway_icon', $icon_html, $this->id );
	}

	/**
	 * Get payment method backend configuration form fields
	 */
	public function get_backend_configuration_form_fields() {
		$form_fields = parent::get_backend_configuration_form_fields();
		$form_fields['card_types'] = array(
			'title' => __( 'BACKEND_CH_MODE', 'wc-genericshop' ),
			'type' => 'multiselect',
			'options' => array(
				'VISA' => __( 'BACKEND_CC_VISA', 'wc-genericshop' ),
				'MASTER' => __( 'BACKEND_CC_MASTER', 'wc-genericshop' ),
				'AMEX' => __( 'BACKEND_CC_AMEX', 'wc-genericshop' ),
				'DINERS' => __( 'BACKEND_CC_DINERS', 'wc-genericshop' ),
				'JCB' => __( 'BACKEND_CC_JCB', 'wc-genericshop' ),
			),
		);
		$form_fields['trans_mode'] = array(
			'title' => __( 'BACKEND_CH_MODE', 'wc-genericshop' ),
			'css'   => 'padding: 1px;',
			'type' => 'select',
			'options' => array(
				'PA' => __( 'BACKEND_CH_MODEPREAUTH', 'wc-genericshop' ),
				'DB' => __( 'BACKEND_CH_MODEDEBIT', 'wc-genericshop' ),
			),
			'default' => 'PA',
		);

		return $form_fields;
	}

	/**
	 * Get payment method title.
	 *
	 * @return string
	 */
	public function get_title() {
		if ( is_admin() ) {
			return __( 'BACKEND_PM_CC', 'wc-genericshop' );
		}

		return __( 'FRONTEND_PM_CC', 'wc-genericshop' );
	}
}

$obj = new Gateway_Genericshop_CC();
