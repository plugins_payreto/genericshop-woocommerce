<?php
/**
 * Genericshop Credit Card Recurring
 *
 * The gateway is used for Credit card Recurring.
 * Copyright (c) Genericshop
 *
 * @class      Gateway_Genericshop_CCSaved
 * @package    Genericshop/Gateway
 * @extends    Genericshop_Payment_Gateway
 * @located at /includes/gateways
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * The gateway is used for Credit card Recurring.
 */
class Gateway_Genericshop_CCSaved extends Gateway_Genericshop_CC {
	/**
	 * Identifier CC
	 *
	 * @var string $id
	 */
	public $id = 'genericshop_ccsaved';

	/**
	 * Payment group
	 *
	 * @var string $payment_group
	 */
	protected $payment_group = 'CC';

	/**
	 * Is recurring
	 *
	 * @var bool $is_recurring
	 */
	protected $is_recurring = true;

	/**
	 * Get payment method backend configuration form fields
	 */
	public function get_backend_configuration_form_fields() {
		$form_fields = parent::get_backend_configuration_form_fields();
		$form_fields['amount_registration'] = array(
			'title' => __( 'BACKEND_CH_AMOUNT', 'wc-genericshop' ),
			'type' => 'text',
			'default' => '',
			'description' => __( 'BACKEND_TT_REGISTRATION_AMOUNT', 'wc-genericshop' ),
		);
		$form_fields['multichannel'] = array(
			'title' => __( 'BACKEND_CH_MULTICHANNEL', 'wc-genericshop' ),
			'css'   => 'padding: 1px;',
			'type' => 'select',
			'options' => array(
				true => __( 'BACKEND_BT_YES', 'wc-genericshop' ),
				false => __( 'BACKEND_BT_NO', 'wc-genericshop' ),
			),
			'default' => 0,
			'description' => __( 'BACKEND_TT_MULTICHANNEL', 'wc-genericshop' ),
		);
		$form_fields['channel_moto'] = array(
			'title' => __( 'BACKEND_CH_MOTO', 'wc-genericshop' ),
			'type' => 'text',
			'default' => '',
			'description' => __( 'BACKEND_TT_CHANNEL_MOTO', 'wc-genericshop' ),
		);

		return $form_fields;
	}

	/**
	 * Get payment method title.
	 *
	 * @return string
	 */
	public function get_title() {
		if ( is_admin() ) {
			return __( 'BACKEND_PM_CCSAVED', 'wc-genericshop' );
		}

		return __( 'FRONTEND_PM_CCSAVED', 'wc-genericshop' );
	}
}

$obj = new Gateway_Genericshop_CCSaved();
