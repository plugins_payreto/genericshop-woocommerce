<?php
/**
 * Generic Shop Version Tracker
 *
 * The class to send version tracker ( every payments transaction )
 * Copyright (c) Generic Shop
 *
 * @class      GenericshopVersionTracker
 * @package  Genericshop/Classes
 * @located at  /includes/core/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * The class to share IP, email address, etc with Genericshop.
 */
class GenericshopVersionTracker {
	/**
	 * Get the version tracker url
	 */
	private static function _get_version_tracker_url() {
		$_version_tracker_url = 'http://api.dbserver.payreto.eu/v1/tracker';
		return $_version_tracker_url;
	}

	/**
	 * Get version tracker parameter
	 *
	 * @param array $version_data version data.
	 * @return array
	 */
	private static function _get_version_tracker_parameter( $version_data ) {
		$version_data['hash'] = md5( $version_data['shop_version'] .
			$version_data['plugin_version'] .
			$version_data['client']
		);

		return http_build_query( array_filter( $version_data ), '', '&' );
	}

	/**
	 * Send version tracker into the API
	 *
	 * @param array $version_data version data.
	 * @return array
	 */
	public static function send_version_tracker( $version_data ) {
		$post_data = self::_get_version_tracker_parameter( $version_data );
		$url = self::_get_version_tracker_url();
		return GenericshopPaymentCore::_get_response_data( $post_data, $url, $version_data['transaction_mode'] );
	}
}
