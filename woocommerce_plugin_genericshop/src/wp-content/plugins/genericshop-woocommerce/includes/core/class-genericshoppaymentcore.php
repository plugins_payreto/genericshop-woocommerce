<?php
/**
 * Generic Shop OPP Core
 *
 * The Class for Process Opp Payment Gateways
 * Copyright (c) Generic Shop
 *
 * @class      GenericshopPaymentCore
 * @package    Genericshop/Classes
 * @located at /includes/core/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * The Class for Proccess Opp Payment Gateways
 */
class GenericshopPaymentCore {
	/**
	 * Checkout url live
	 *
	 * @var string $_checkout_url_live
	 */
	protected static $_checkout_url_live = 'https://oppwa.com/v1/checkouts';
	/**
	 * Checkout url test
	 *
	 * @var string $_checkout_url_test
	 */
	protected static $_checkout_url_test = 'https://test.oppwa.com/v1/checkouts';

	/**
	 * Backoffice url live
	 *
	 * @var string $_back_office_url_live
	 */
	protected static $_back_office_url_live = 'https://oppwa.com/v1/payments/';
	/**
	 * Backoffice url test
	 *
	 * @var string $_back_office_url_test
	 */
	protected static $_back_office_url_test = 'https://test.oppwa.com/v1/payments/';

	/**
	 * Payment widget url live
	 *
	 * @var string $_payment_widget_url_live
	 */
	protected static $_payment_widget_url_live = 'https://oppwa.com/v1/paymentWidgets.js?checkoutId=';
	/**
	 * Payment widget url test
	 *
	 * @var string $_payment_widget_url_test
	 */
	protected static $_payment_widget_url_test = 'https://test.oppwa.com/v1/paymentWidgets.js?checkoutId=';

	/**
	 * Register url live
	 *
	 * @var string $_register_url_live
	 */
	protected static $_register_url_live = 'https://oppwa.com/v1/registrations/';
	/**
	 * Register url test
	 *
	 * @var string $_register_url_test
	 */
	protected static $_register_url_test = 'https://test.oppwa.com/v1/registrations/';

	/**
	 * Query url live
	 *
	 * @var string $_query_url_live
	 */
	protected static $_query_url_live = 'https://ctpe.net/payment/query';
	/**
	 * Query url test
	 *
	 * @var string $_query_url_test
	 */
	protected static $_query_url_test = 'https://test.ctpe.net/payment/query';

	/**
	 * ACK return codes
	 *
	 * @var array $_ack_return_codes
	 */
	protected static $_ack_return_codes = array(
		'000.000.000',
		'000.100.110',
		'000.100.111',
		'000.100.112',
		'000.100.200',
		'000.100.201',
		'000.100.202',
		'000.100.203',
		'000.100.204',
		'000.100.205',
		'000.100.206',
		'000.100.207',
		'000.100.208',
		'000.100.209',
		'000.100.210',
		'000.100.220',
		'000.100.221',
		'000.100.222',
		'000.100.223',
		'000.100.224',
		'000.100.225',
		'000.100.226',
		'000.100.227',
		'000.100.228',
		'000.100.229',
		'000.100.230',
		'000.100.299',
		'000.200.000',
		'000.300.000',
		'000.300.100',
		'000.300.101',
		'000.300.102',
		'000.400.000',
		'000.400.010',
		'000.400.020',
		'000.400.030',
		'000.400.040',
		'000.400.050',
		'000.400.060',
		'000.400.070',
		'000.400.080',
		'000.400.090',
		'000.400.101',
		'000.400.102',
		'000.400.103',
		'000.400.104',
		'000.400.105',
		'000.400.106',
		'000.400.107',
		'000.400.108',
		'000.400.200',
		'000.500.000',
		'000.500.100',
		'000.600.000',
	);

	/**
	 * NOK return codes
	 *
	 * @var array $_nok_return_codes
	 */
	protected static $_nok_return_codes = array(
		'100.100.100',
		'100.100.101',
		'100.100.200',
		'100.100.201',
		'100.100.300',
		'100.100.301',
		'100.100.303',
		'100.100.304',
		'100.100.400',
		'100.100.401',
		'100.100.402',
		'100.100.500',
		'100.100.501',
		'100.100.600',
		'100.100.601',
		'100.100.650',
		'100.100.651',
		'100.100.700',
		'100.100.701',
		'100.150.100',
		'100.150.101',
		'100.150.200',
		'100.150.201',
		'100.150.202',
		'100.150.203',
		'100.150.204',
		'100.150.205',
		'100.150.300',
		'100.200.100',
		'100.200.103',
		'100.200.104',
		'100.200.200',
		'100.210.101',
		'100.210.102',
		'100.211.101',
		'100.211.102',
		'100.211.103',
		'100.211.104',
		'100.211.105',
		'100.211.106',
		'100.212.101',
		'100.212.102',
		'100.212.103',
		'100.250.100',
		'100.250.105',
		'100.250.106',
		'100.250.107',
		'100.250.110',
		'100.250.111',
		'100.250.120',
		'100.250.121',
		'100.250.122',
		'100.250.123',
		'100.250.124',
		'100.250.125',
		'100.250.250',
		'100.300.101',
		'100.300.200',
		'100.300.300',
		'100.300.400',
		'100.300.401',
		'100.300.402',
		'100.300.501',
		'100.300.600',
		'100.300.601',
		'100.300.700',
		'100.300.701',
		'100.350.100',
		'100.350.101',
		'100.350.200',
		'100.350.201',
		'100.350.301',
		'100.350.302',
		'100.350.303',
		'100.350.310',
		'100.350.311',
		'100.350.312',
		'100.350.313',
		'100.350.314',
		'100.350.315',
		'100.350.400',
		'100.350.500',
		'100.350.600',
		'100.350.601',
		'100.350.610',
		'100.360.201',
		'100.360.300',
		'100.360.303',
		'100.360.400',
		'100.370.100',
		'100.370.101',
		'100.370.102',
		'100.370.110',
		'100.370.111',
		'100.370.121',
		'100.370.122',
		'100.370.123',
		'100.370.124',
		'100.370.125',
		'100.370.131',
		'100.370.132',
		'100.380.100',
		'100.380.101',
		'100.380.110',
		'100.380.201',
		'100.380.305',
		'100.380.306',
		'100.380.401',
		'100.380.501',
		'100.390.101',
		'100.390.102',
		'100.390.103',
		'100.390.104',
		'100.390.105',
		'100.390.106',
		'100.390.107',
		'100.390.108',
		'100.390.109',
		'100.390.110',
		'100.390.111',
		'100.390.112',
		'100.390.113',
		'100.395.101',
		'100.395.102',
		'100.395.501',
		'100.395.502',
		'100.396.101',
		'100.396.102',
		'100.396.103',
		'100.396.104',
		'100.396.106',
		'100.396.201',
		'100.397.101',
		'100.397.102',
		'100.400.000',
		'100.400.001',
		'100.400.002',
		'100.400.005',
		'100.400.007',
		'100.400.020',
		'100.400.021',
		'100.400.030',
		'100.400.039',
		'100.400.040',
		'100.400.041',
		'100.400.042',
		'100.400.043',
		'100.400.044',
		'100.400.045',
		'100.400.051',
		'100.400.060',
		'100.400.061',
		'100.400.063',
		'100.400.064',
		'100.400.065',
		'100.400.071',
		'100.400.080',
		'100.400.081',
		'100.400.083',
		'100.400.084',
		'100.400.085',
		'100.400.086',
		'100.400.087',
		'100.400.091',
		'100.400.100',
		'100.400.120',
		'100.400.121',
		'100.400.122',
		'100.400.123',
		'100.400.130',
		'100.400.139',
		'100.400.140',
		'100.400.141',
		'100.400.142',
		'100.400.143',
		'100.400.144',
		'100.400.145',
		'100.400.146',
		'100.400.147',
		'100.400.148',
		'100.400.149',
		'100.400.150',
		'100.400.151',
		'100.400.152',
		'100.400.241',
		'100.400.242',
		'100.400.243',
		'100.400.260',
		'100.400.300',
		'100.400.301',
		'100.400.302',
		'100.400.303',
		'100.400.304',
		'100.400.305',
		'100.400.306',
		'100.400.307',
		'100.400.308',
		'100.400.309',
		'100.400.310',
		'100.400.311',
		'100.400.312',
		'100.400.313',
		'100.400.314',
		'100.400.315',
		'100.400.316',
		'100.400.317',
		'100.400.318',
		'100.400.319',
		'100.400.320',
		'100.400.321',
		'100.400.322',
		'100.400.323',
		'100.400.324',
		'100.400.325',
		'100.400.326',
		'100.400.327',
		'100.400.328',
		'100.400.500',
		'100.500.101',
		'100.500.201',
		'100.500.301',
		'100.500.302',
		'100.550.300',
		'100.550.301',
		'100.550.303',
		'100.550.310',
		'100.550.311',
		'100.550.312',
		'100.550.400',
		'100.550.401',
		'100.550.601',
		'100.550.603',
		'100.550.605',
		'100.600.500',
		'100.700.100',
		'100.700.101',
		'100.700.200',
		'100.700.201',
		'100.700.300',
		'100.700.400',
		'100.700.500',
		'100.700.800',
		'100.700.801',
		'100.700.802',
		'100.700.810',
		'100.800.100',
		'100.800.101',
		'100.800.102',
		'100.800.200',
		'100.800.201',
		'100.800.202',
		'100.800.300',
		'100.800.301',
		'100.800.302',
		'100.800.400',
		'100.800.401',
		'100.800.500',
		'100.800.501',
		'100.900.100',
		'100.900.101',
		'100.900.105',
		'100.900.200',
		'100.900.300',
		'100.900.301',
		'100.900.400',
		'100.900.401',
		'100.900.450',
		'100.900.500',
		'200.100.101',
		'200.100.102',
		'200.100.103',
		'200.100.150',
		'200.100.151',
		'200.100.199',
		'200.100.201',
		'200.100.300',
		'200.100.301',
		'200.100.302',
		'200.100.401',
		'200.100.402',
		'200.100.403',
		'200.100.404',
		'200.100.501',
		'200.100.502',
		'200.100.503',
		'200.100.504',
		'200.200.106',
		'200.300.403',
		'200.300.404',
		'200.300.405',
		'200.300.406',
		'200.300.407',
		'500.100.201',
		'500.100.202',
		'500.100.203',
		'500.100.301',
		'500.100.302',
		'500.100.303',
		'500.100.304',
		'500.100.401',
		'500.100.402',
		'500.100.403',
		'500.200.101',
		'600.100.100',
		'600.200.100',
		'600.200.200',
		'600.200.201',
		'600.200.202',
		'600.200.300',
		'600.200.310',
		'600.200.400',
		'600.200.500',
		'600.200.600',
		'600.200.700',
		'600.200.800',
		'600.200.810',
		'700.100.100',
		'700.100.200',
		'700.100.300',
		'700.100.400',
		'700.100.500',
		'700.100.600',
		'700.100.700',
		'700.100.701',
		'700.100.710',
		'700.300.100',
		'700.300.200',
		'700.300.300',
		'700.300.400',
		'700.300.500',
		'700.300.600',
		'700.300.700',
		'700.400.000',
		'700.400.100',
		'700.400.101',
		'700.400.200',
		'700.400.300',
		'700.400.400',
		'700.400.402',
		'700.400.410',
		'700.400.420',
		'700.400.510',
		'700.400.520',
		'700.400.530',
		'700.400.540',
		'700.400.550',
		'700.400.560',
		'700.400.561',
		'700.400.562',
		'700.400.570',
		'700.400.700',
		'700.450.001',
		'800.100.100',
		'800.100.150',
		'800.100.151',
		'800.100.152',
		'800.100.153',
		'800.100.154',
		'800.100.155',
		'800.100.156',
		'800.100.157',
		'800.100.158',
		'800.100.159',
		'800.100.160',
		'800.100.161',
		'800.100.162',
		'800.100.163',
		'800.100.164',
		'800.100.165',
		'800.100.166',
		'800.100.167',
		'800.100.168',
		'800.100.169',
		'800.100.170',
		'800.100.171',
		'800.100.172',
		'800.100.173',
		'800.100.174',
		'800.100.175',
		'800.100.176',
		'800.100.177',
		'800.100.178',
		'800.100.179',
		'800.100.190',
		'800.100.191',
		'800.100.192',
		'800.100.195',
		'800.100.196',
		'800.100.197',
		'800.100.198',
		'800.100.402',
		'800.100.500',
		'800.100.501',
		'800.110.100',
		'800.120.100',
		'800.120.101',
		'800.120.102',
		'800.120.103',
		'800.120.200',
		'800.120.201',
		'800.120.202',
		'800.120.203',
		'800.120.300',
		'800.120.401',
		'800.121.100',
		'800.130.100',
		'800.140.100',
		'800.140.101',
		'800.140.110',
		'800.140.111',
		'800.140.112',
		'800.140.113',
		'800.150.100',
		'800.160.100',
		'800.160.110',
		'800.160.120',
		'800.160.130',
		'800.200.159',
		'800.200.160',
		'800.200.165',
		'800.200.202',
		'800.200.208',
		'800.200.220',
		'800.300.101',
		'800.300.102',
		'800.300.200',
		'800.300.301',
		'800.300.302',
		'800.300.401',
		'800.300.500',
		'800.300.501',
		'800.400.100',
		'800.400.101',
		'800.400.102',
		'800.400.103',
		'800.400.104',
		'800.400.105',
		'800.400.110',
		'800.400.150',
		'800.400.151',
		'800.400.200',
		'800.400.500',
		'800.500.100',
		'800.500.110',
		'800.600.100',
		'800.700.100',
		'800.700.101',
		'800.700.201',
		'800.700.500',
		'800.800.102',
		'800.800.202',
		'800.800.302',
		'800.800.800',
		'800.800.801',
		'800.900.100',
		'800.900.101',
		'800.900.200',
		'800.900.201',
		'800.900.300',
		'800.900.301',
		'800.900.302',
		'800.900.303',
		'800.900.401',
		'800.900.450',
		'900.100.100',
		'900.100.200',
		'900.100.201',
		'900.100.202',
		'900.100.203',
		'900.100.300',
		'900.100.400',
		'900.100.500',
		'900.100.600',
		'900.200.100',
		'900.300.600',
		'900.400.100',
		'999.999.999',
	);

	/**
	 * OPP URL
	 */

	/**
	 * Get the URL to checkout payment based on the server mode
	 *
	 * @param string $server_mode server mode.
	 * @return string
	 */
	private static function _get_checkout_url( $server_mode ) {
		if ( 'LIVE' === $server_mode ) {
			return self::$_checkout_url_live;
		} else {
			return self::$_checkout_url_test;
		}
	}

	/**
	 * Get the payment status URL based on the server mode
	 *
	 * @param string $server_mode server mode.
	 * @param string $checkout_id checkout id.
	 * @return string
	 */
	private static function _get_payment_status_url( $server_mode, $checkout_id ) {
		if ( 'LIVE' === $server_mode ) {
			return self::$_checkout_url_live . '/' . $checkout_id . '/payment';
		} else {
			return self::$_checkout_url_test . '/' . $checkout_id . '/payment';
		}
	}

	/**
	 * Get the back office URL based on the server mode
	 *
	 * @param string $server_mode server mode.
	 * @param string $reference_id reference id.
	 * @return string
	 */
	private static function _get_back_office_url( $server_mode, $reference_id ) {
		if ( 'LIVE' === $server_mode ) {
			return self::$_back_office_url_live . $reference_id;
		} else {
			return self::$_back_office_url_test . $reference_id;
		}
	}

	/**
	 * Get the URL to use registered account based on the server mode
	 *
	 * @param string $server_mode server mode.
	 * @param string $reference_id reference id.
	 * @return string
	 */
	private static function _get_url_to_use_registered_account( $server_mode, $reference_id ) {
		if ( 'LIVE' === $server_mode ) {
			return self::$_register_url_live . $reference_id . '/payments';
		} else {
			return self::$_register_url_test . $reference_id . '/payments';
		}
	}

	/**
	 * Get the URL to deregister payment account based on the server mode
	 *
	 * @param string $server_mode server mode.
	 * @param string $reference_id reference id.
	 * @return string
	 */
	private static function _get_deregister_url( $server_mode, $reference_id ) {
		if ( 'LIVE' === $server_mode ) {
			return self::$_register_url_live . $reference_id;
		} else {
			return self::$_register_url_test . $reference_id;
		}
	}

	/**
	 * Get the query URL based on the server mode
	 *
	 * @param string $server_mode server mode.
	 * @return string
	 */
	private static function _get_query_url( $server_mode ) {
		if ( 'LIVE' === $server_mode ) {
			return self::$_query_url_live;
		} else {
			return self::$_query_url_test;
		}
	}

	/**
	 * OPP Remote
	 */

	/**
	 * Get value for wp_remote parameter sslverify.
	 * true when server mode LIVE and false when server mode TEST.
	 *
	 * @param string $server_mode server mode.
	 * @return bool
	 */
	private static function _get_ssl_verify( $server_mode ) {
		if ( 'TEST' === $server_mode ) {
			return false;
		}
		return true;
	}

	/**
	 * Get wp response
	 *
	 * @param wp_remote $response response.
	 * @return array
	 */
	private static function _get_wp_response( $response ) {
		if ( is_wp_error( $response ) ) {
			$result['response'] = 'ERROR_UNKNOWN';
			$result['is_valid'] = false;
			$error_message = $response->get_error_message();
			if ( false !== strpos( $error_message, 'cURL error 60' ) ) {
				Genericshop_General_Functions::add_log( 'curl error message : ', $error_message );
				$result['response'] = 'ERROR_MERCHANT_SSL_CERTIFICATE';
			}
			return $result;
		}

		$response = wp_remote_retrieve_body( $response );
		Genericshop_General_Functions::add_log( 'curl response : ', $response );
		$json_response = json_decode( $response, true );
		if ( isset( $json_response ) ) {
			$result['response'] = $json_response;
		} else {
			$result['response'] = $response;
		}
		$result['is_valid'] = true;

		return $result;
	}

	/**
	 * Get response data
	 *
	 * @param array  $data post data.
	 * @param string $url url.
	 * @param string $server_mode server mode.
	 * @return array
	 */
	public static function _get_response_data( $data, $url, $server_mode = 'LIVE' ) {
		Genericshop_General_Functions::add_log( 'request to url : ', $url );
		Genericshop_General_Functions::add_log( 'request server mode : ', $server_mode );
		Genericshop_General_Functions::add_log( 'request data : ', $data );

		$response = wp_remote_post( $url,
			array(
				'body' => $data,
				'sslverify' => self::_get_ssl_verify( $server_mode ),
				'timeout' => 100,
			)
		);
		return self::_get_wp_response( $response );
	}

	/**
	 * Get payment response from the gateway
	 *
	 * @param string $url url.
	 * @param string $server_mode server mode.
	 * @return array
	 */
	private static function _get_payment_response( $url, $server_mode = 'LIVE' ) {
		$response = wp_remote_get( $url,
			array(
				'sslverify' => self::_get_ssl_verify( $server_mode ),
			)
		); // Ok.
		return self::_get_wp_response( $response );
	}

	/**
	 * Send deregistration payment account to the gateway
	 *
	 * @param string $url url.
	 * @param string $server_mode server mode.
	 * @return array
	 */
	private static function _send_deregistration( $url, $server_mode = 'LIVE' ) {
		$response = wp_remote_get( $url,
			array(
				'sslverify' => self::_get_ssl_verify( $server_mode ),
				'method' => 'DELETE',
			)
		);
		return self::_get_wp_response( $response );
	}

	/**
	 * Get a current payment status
	 *
	 * @param string $url url.
	 * @param string $xml_request xml request.
	 * @param string $server_mode server mode.
	 * @return array
	 */
	private static function _get_current_payment_status( $url, $xml_request, $server_mode = 'LIVE' ) {
		$response = wp_remote_post( $url,
			array(
				'headers' => array(
					'content-type' => 'application/x-www-form-urlencoded',
					'charset' => 'UTF-8',
				),
				'body' => $xml_request,
				'sslverify' => self::_get_ssl_verify( $server_mode ),
				'timeout' => 100,
			)
		);
		return self::_get_wp_response( $response );
	}

	/**
	 * Get the payment credential for send request to the gateway
	 *
	 * @param array $order transaction parameters.
	 * @return array
	 */
	private static function _get_payment_credential( $order ) {
		$payment_credential = array();
		$payment_credential['authentication.userId'] = $order['login'];
		$payment_credential['authentication.password'] = $order['password'];
		$payment_credential['authentication.entityId'] = $order['channel_id'];

		// test mode payment_credential ( true ).
		if ( ! empty( $order['test_mode'] ) ) {
			$payment_credential['testMode'] = $order['test_mode'];
		}

		return $payment_credential;
	}


	/**
	 * Sets the separator for the decimal point
	 * Sets the number of decimal points
	 *
	 * @param string $number number.
	 * @return string
	 */
	public static function set_number_format( $number ) {
		$number = (float) str_replace( ',', '.', $number );

		return number_format( $number, 2, '.', '' );
	}

	/**
	 * Set cart items parameters
	 *
	 * @param array $cart_items cart items.
	 * @return array
	 */
	private static function _set_cart_items_parameters( $cart_items ) {
		$parameters = array();
		$count_cart_items = count( $cart_items );
		for ( $i = 0; $i < $count_cart_items; $i++ ) {
			$parameters[ 'cart.items[' . $i . '].merchantItemId' ] = $cart_items[ $i ]['merchant_item_id'];
			if ( ! empty( $cart_items[ $i ]['discount'] ) ) {
				$parameters[ 'cart.items[' . $i . '].discount' ] = self::set_number_format( $cart_items[ $i ]['discount'] );
			}
			$parameters[ 'cart.items[' . $i . '].quantity' ] = $cart_items[ $i ]['quantity'];
			$parameters[ 'cart.items[' . $i . '].name' ] = $cart_items[ $i ]['name'];
			$parameters[ 'cart.items[' . $i . '].price' ] = self::set_number_format( $cart_items[ $i ]['price'] );
			if ( ! empty( $cart_items[ $i ]['tax'] ) ) {
				$parameters[ 'cart.items[' . $i . '].tax' ] = self::set_number_format( $cart_items[ $i ]['tax'] );
			}
		}
		return $parameters;
	}

	/**
	 * Set checkout parameters
	 *
	 * @param array $order transaction parameters.
	 * @return string
	 */
	private static function _set_checkout_parameters( $order ) {
		$parameters = array();
		$parameters = self::_get_payment_credential( $order );
		$parameters['merchantTransactionId'] = $order['transaction_id'];
		$parameters['customer.email'] = $order['customer']['email'];
		$parameters['customer.givenName'] = $order['customer']['first_name'];
		$parameters['customer.surname'] = $order['customer']['last_name'];
		$parameters['billing.street1'] = $order['billing']['street'];
		$parameters['billing.city'] = $order['billing']['city'];
		$parameters['billing.postcode'] = $order['billing']['zip'];
		$parameters['billing.country'] = $order['billing']['country_code'];

		if ( isset( $order['shipping']['street'] ) ) {
			$parameters['shipping.street1'] = $order['shipping']['street'];
		}
		if ( isset( $order['shipping']['city'] ) ) {
			$parameters['shipping.city'] = $order['shipping']['city'];
		}
		if ( isset( $order['shipping']['zip'] ) ) {
			$parameters['shipping.postcode'] = $order['shipping']['zip'];
		}
		if ( isset( $order['shipping']['country_code'] ) ) {
			$parameters['shipping.country'] = $order['shipping']['country_code'];
		}
		$parameters['amount'] = self::set_number_format( $order['amount'] );
		$parameters['currency'] = $order['currency'];

		if ( isset( $order['customer']['sex'] ) ) {
			$parameters['customer.sex'] = $order['customer']['sex'];
		}
		if ( isset( $order['customer']['birthdate'] ) && '0000-00-00' !== $order['customer']['birthdate'] ) {
			$parameters['customer.birthDate'] = $order['customer']['birthdate'];
		}
		if ( isset( $order['customer']['phone'] ) ) {
			$parameters['customer.phone'] = $order['customer']['phone'];
		}
		if ( isset( $order['customer']['mobile'] ) ) {
			$parameters['customer.mobile'] = $order['customer']['mobile'];
		}

		// payment type for RG.DB or only RG.
		if ( ! empty( $order['payment_type'] ) ) {
			$parameters['paymentType'] = $order['payment_type'];
		}

		if ( isset( $order['payment_brand'] ) && 'RATENKAUF' === $order['payment_brand'] ) {
			$parameters['paymentBrand'] = $order['payment_brand'];
		}

		// registration parameter ( true ).
		if ( ! empty( $order['payment_registration'] ) ) {
			$parameters['createRegistration'] = $order['payment_registration'];
			if ( ! empty( $order['3D'] ) ) {
				  $parameters['customParameters[presentation.amount3D]'] = self::set_number_format( $order['3D']['amount'] );
				  $parameters['customParameters[presentation.currency3D]'] = $order['3D']['currency'];
			}
		}

		// recurring payment parameters : initial/repeated.
		if ( ! empty( $order['payment_recurring'] ) ) {
			$parameters['recurringType'] = $order['payment_recurring'];
		}

		if ( ! empty( $order['customer_ip'] ) ) {
			$parameters['customer.ip'] = $order['customer_ip'];
		}

		if ( ! empty( $order['registrations'] ) ) {
			foreach ( $order['registrations'] as $key => $value ) {
				  $parameters[ 'registrations[' . $key . '].id' ] = $value;
			}
		}

		return $parameters;
	}

	/**
	 * Set parameters to use registered payment account
	 *
	 * @param array $order transaction parameters.
	 * @return string
	 */
	private static function _set_registered_account_parameters( $order ) {
		$parameters = self::_get_payment_credential( $order );
		$parameters['amount'] = self::set_number_format( $order['amount'] );
		$parameters['currency'] = $order['currency'];
		$parameters['paymentType'] = $order['payment_type'];
		$parameters['merchantTransactionId'] = $order['transaction_id'];
		$parameters['recurringType'] = $order['payment_recurring'];

		return $parameters;
	}

	/**
	 * Set back office parameters
	 *
	 * @param array $order transaction parameters.
	 * @return string
	 */
	private static function _set_back_office_parameters( $order ) {
		$parameters = self::_get_payment_credential( $order );
		$parameters['paymentType'] = $order['payment_type'];

		// Reversal ( RV ) didn't send amount & currency parameter.
		if ( 'RV' !== $order['payment_type'] ) {
			$parameters['amount'] = self::set_number_format( $order['amount'] );
			$parameters['currency'] = $order['currency'];
		}

		return $parameters;
	}

	/**
	 * Get XML current payment status
	 *
	 * @param string $reference_id reference id.
	 * @param array  $order transaction parameters.
	 * @return string
	 */
	private static function _get_xml_current_payment_status( $reference_id, $order ) {
		switch ( $order['test_mode'] ) {
			case 'INTERNAL':
				$mode = 'INTEGRATOR_TEST';
				break;
			case 'EXTERNAL':
				$mode = 'CONNECTOR_TEST';
				break;
			case 'LIVE':
				$mode = 'LIVE';
				break;
		}

		$xml = 'load=<?xml version="1.0" encoding="UTF-8"?><Request version="1.0">
			<Header><Security sender="' . $order['channel_id'] . '"/></Header>
			<Query mode="' . $mode . '" level="CHANNEL" entity="' . $order['channel_id'] . '" type="STANDARD">
				<User login="' . $order['login'] . '" pwd="' . $order['password'] . '"/>
				<Identification>
					<UniqueID>' . $reference_id . '</UniqueID>
				</Identification>
				</Query>
			</Request>';

		return $xml;
	}

	/**
	 * OPP Main Operation
	 */

	/**
	 * Check if the plugin gets a response from the gateway in 3 trials
	 *
	 * @param string $payment_status_url payment status url.
	 * @param json   $server_mode server mode.
	 * @return array
	 */
	private static function _is_payment_get_response( $payment_status_url, $server_mode ) {
		for ( $i = 0; $i < 3; $i++ ) {
			$response = true;
			try {
				$payment_response = self::_get_payment_response( $payment_status_url, $server_mode );
			} catch ( Exception $e ) {
				$response = false;
			}
			if ( $response && $payment_response['is_valid'] ) {
				return $payment_response;
			}
		}

		$payment_response['is_valid'] = false;
		if ( ! isset( $payment_response['response'] ) ) {
			$payment_response['response'] = 'ERROR_UNKNOWN';
		}

		return $payment_response;
	}

	/**
	 * Prepare the checkout & Get payment widget url ( for create payment form )
	 *
	 * @param array $order transaction parameters.
	 * @return array
	 */
	public static function get_checkout_result( $order ) {
		$checkout_url = self::_get_checkout_url( $order['server_mode'] );
		$post_data = self::_set_checkout_parameters( $order );

		return self::_get_response_data( $post_data, $checkout_url, $order['server_mode'] );
	}

	/**
	 * Get payment widget url ( for create payment form )
	 *
	 * @param string $server_mode server mode.
	 * @param string $checkout_id checkout id.
	 * @return string
	 */
	public static function get_payment_widget_url( $server_mode, $checkout_id ) {
		if ( 'LIVE' === $server_mode ) {
			return self::$_payment_widget_url_live . $checkout_id;
		} else {
			return self::$_payment_widget_url_test . $checkout_id;
		}
	}

	/**
	 * Get a payment status
	 *
	 * @param string $checkout_id checkout id.
	 * @param array  $order transaction parameters.
	 * @return array|boolean
	 */
	public static function get_payment_status( $checkout_id, $order ) {
		$payment_status_url = self::_get_payment_status_url( $order['server_mode'], $checkout_id );
		$payment_status_url .= '?' . http_build_query( self::_get_payment_credential( $order ), '', '&' );

		return self::_is_payment_get_response( $payment_status_url, $order['server_mode'] );
	}

	/**
	 * Get payment widget content
	 *
	 * @param string $url payment widget url.
	 * @param array  $server_mode server mode.
	 * @return array
	 */
	public static function get_payment_widget_content( $url, $server_mode ) {
		$response = wp_remote_get( $url,
			array(
				'sslverify' => self::_get_ssl_verify( $server_mode ),
			)
		);
		return self::_get_wp_response( $response );
	}

	/**
	 * Back office operation such as Capture, Refund, and Reversal
	 *
	 * @param string $reference_id reference id.
	 * @param array  $order transaction parameters.
	 * @return array
	 */
	public static function back_office_operation( $reference_id, $order ) {
		$back_office_url = self::_get_back_office_url( $order['server_mode'], $reference_id );
		$post_data = self::_set_back_office_parameters( $order );

		return self::_get_response_data( $post_data, $back_office_url, $order['server_mode'] );
	}

	/**
	 * Use the registered payment account
	 *
	 * @param string $reference_id reference id.
	 * @param array  $order transaction parameters.
	 * @return array
	 */
	public static function use_registered_account( $reference_id, $order ) {
		$registered_account_url = self::_get_url_to_use_registered_account( $order['server_mode'], $reference_id );
		$post_data = self::_set_registered_account_parameters( $order );

		return self::_get_response_data( $post_data, $registered_account_url, $order['server_mode'] );
	}

	/**
	 * Delete registration
	 *
	 * @param string $reference_id reference id.
	 * @param array  $order transaction parameters.
	 * @return array
	 */
	public static function delete_registered_account( $reference_id, $order ) {
		$deregister_url = self::_get_deregister_url( $order['server_mode'], $reference_id );
		$deregister_url .= '?' . http_build_query( self::_get_payment_credential( $order ), '', '&' );

		return self::_send_deregistration( $deregister_url, $order['server_mode'] );
	}

	/**
	 * Update payment status
	 *
	 * @param string $reference_id reference id.
	 * @param array  $order transaction parameters.
	 * @return xml
	 */
	public static function update_status( $reference_id, $order ) {
		$query_url = self::_get_query_url( $order['server_mode'] );
		$xml_current_payment_status = self::_get_xml_current_payment_status( $reference_id, $order );

		return self::_get_current_payment_status( $query_url, $xml_current_payment_status, $order['server_mode'] );
	}

	/**
	 * Get transaction result ACK, NOK, or false
	 *
	 * @param bool $return_code code from respond.
	 * @return string
	 */
	public static function get_transaction_result( $return_code = false ) {
		if ( $return_code ) {
			if ( in_array( $return_code, self::$_ack_return_codes, true ) ) {
				return 'ACK';
			} elseif ( in_array( $return_code, self::$_nok_return_codes, true ) ) {
				return 'NOK';
			}
		}
		return false;
	}

	/**
	 * Get error identifier
	 *
	 * @param string $code code from respond.
	 * @return string
	 */
	public static function get_error_identifier( $code ) {
		$error_messages = array(
			'800.150.100' => 'ERROR_CC_ACCOUNT',

			'800.100.402' => 'ERROR_CC_INVALIDDATA',
			'100.100.101' => 'ERROR_CC_INVALIDDATA',
			'800.100.151' => 'ERROR_CC_INVALIDDATA',
			'000.400.108' => 'ERROR_CC_INVALIDDATA',
			'100.100.100' => 'ERROR_CC_INVALIDDATA',
			'100.100.200' => 'ERROR_CC_INVALIDDATA',
			'100.100.201' => 'ERROR_CC_INVALIDDATA',
			'100.100.300' => 'ERROR_CC_INVALIDDATA',
			'100.100.301' => 'ERROR_CC_INVALIDDATA',
			'100.100.304' => 'ERROR_CC_INVALIDDATA',
			'100.100.400' => 'ERROR_CC_INVALIDDATA',
			'100.100.401' => 'ERROR_CC_INVALIDDATA',
			'100.100.402' => 'ERROR_CC_INVALIDDATA',
			'100.100.651' => 'ERROR_CC_INVALIDDATA',
			'100.100.700' => 'ERROR_CC_INVALIDDATA',
			'100.200.100' => 'ERROR_CC_INVALIDDATA',
			'100.200.103' => 'ERROR_CC_INVALIDDATA',
			'100.200.104' => 'ERROR_CC_INVALIDDATA',
			'100.400.000' => 'ERROR_CC_INVALIDDATA',
			'100.400.001' => 'ERROR_CC_INVALIDDATA',
			'100.400.086' => 'ERROR_CC_INVALIDDATA',
			'100.400.087' => 'ERROR_CC_INVALIDDATA',
			'100.400.002' => 'ERROR_CC_INVALIDDATA',
			'100.400.316' => 'ERROR_CC_INVALIDDATA',
			'100.400.317' => 'ERROR_CC_INVALIDDATA',
			'100.100.600' => 'ERROR_CC_INVALIDDATA',

			'800.300.401' => 'ERROR_CC_BLACKLIST',

			'800.100.171' => 'ERROR_CC_DECLINED_CARD',
			'800.100.165' => 'ERROR_CC_DECLINED_CARD',
			'800.100.159' => 'ERROR_CC_DECLINED_CARD',
			'800.100.195' => 'ERROR_CC_DECLINED_CARD',
			'000.400.101' => 'ERROR_CC_DECLINED_CARD',
			'100.100.501' => 'ERROR_CC_DECLINED_CARD',
			'100.100.701' => 'ERROR_CC_DECLINED_CARD',
			'100.400.005' => 'ERROR_CC_DECLINED_CARD',
			'100.400.020' => 'ERROR_CC_DECLINED_CARD',
			'100.400.021' => 'ERROR_CC_DECLINED_CARD',
			'100.400.030' => 'ERROR_CC_DECLINED_CARD',
			'100.400.039' => 'ERROR_CC_DECLINED_CARD',
			'100.400.081' => 'ERROR_CC_DECLINED_CARD',
			'100.400.100' => 'ERROR_CC_DECLINED_CARD',
			'100.400.123' => 'ERROR_CC_DECLINED_CARD',
			'100.400.319' => 'ERROR_CC_DECLINED_CARD',
			'800.100.154' => 'ERROR_CC_DECLINED_CARD',
			'800.100.156' => 'ERROR_CC_DECLINED_CARD',
			'800.100.158' => 'ERROR_CC_DECLINED_CARD',
			'800.100.160' => 'ERROR_CC_DECLINED_CARD',
			'800.100.161' => 'ERROR_CC_DECLINED_CARD',
			'800.100.163' => 'ERROR_CC_DECLINED_CARD',
			'800.100.164' => 'ERROR_CC_DECLINED_CARD',
			'800.100.166' => 'ERROR_CC_DECLINED_CARD',
			'800.100.167' => 'ERROR_CC_DECLINED_CARD',
			'800.100.169' => 'ERROR_CC_DECLINED_CARD',
			'800.100.170' => 'ERROR_CC_DECLINED_CARD',
			'800.100.173' => 'ERROR_CC_DECLINED_CARD',
			'800.100.174' => 'ERROR_CC_DECLINED_CARD',
			'800.100.175' => 'ERROR_CC_DECLINED_CARD',
			'800.100.176' => 'ERROR_CC_DECLINED_CARD',
			'800.100.177' => 'ERROR_CC_DECLINED_CARD',
			'800.100.190' => 'ERROR_CC_DECLINED_CARD',
			'800.100.191' => 'ERROR_CC_DECLINED_CARD',
			'800.100.196' => 'ERROR_CC_DECLINED_CARD',
			'800.100.197' => 'ERROR_CC_DECLINED_CARD',
			'800.100.168' => 'ERROR_CC_DECLINED_CARD',

			'100.100.303' => 'ERROR_CC_EXPIRED',

			'800.100.153' => 'ERROR_CC_INVALIDCVV',
			'100.100.601' => 'ERROR_CC_INVALIDCVV',
			'800.100.192' => 'ERROR_CC_INVALIDCVV',

			'800.100.157' => 'ERROR_CC_EXPIRY',

			'800.100.162' => 'ERROR_CC_LIMIT_EXCEED',

			'100.400.040' => 'ERROR_CC_3DAUTH',
			'100.400.060' => 'ERROR_CC_3DAUTH',
			'100.400.080' => 'ERROR_CC_3DAUTH',
			'100.400.120' => 'ERROR_CC_3DAUTH',
			'100.400.260' => 'ERROR_CC_3DAUTH',
			'800.900.300' => 'ERROR_CC_3DAUTH',
			'800.900.301' => 'ERROR_CC_3DAUTH',
			'800.900.302' => 'ERROR_CC_3DAUTH',
			'100.380.401' => 'ERROR_CC_3DAUTH',

			'100.390.105' => 'ERROR_CC_3DERROR',
			'000.400.103' => 'ERROR_CC_3DERROR',
			'000.400.104' => 'ERROR_CC_3DERROR',
			'100.390.106' => 'ERROR_CC_3DERROR',
			'100.390.107' => 'ERROR_CC_3DERROR',
			'100.390.108' => 'ERROR_CC_3DERROR',
			'100.390.109' => 'ERROR_CC_3DERROR',
			'100.390.111' => 'ERROR_CC_3DERROR',
			'800.400.200' => 'ERROR_CC_3DERROR',
			'100.390.112' => 'ERROR_CC_3DERROR',

			'100.100.500' => 'ERROR_CC_NOBRAND',

			'800.100.155' => 'ERROR_GENERAL_LIMIT_AMOUNT',
			'000.100.203' => 'ERROR_GENERAL_LIMIT_AMOUNT',
			'100.550.310' => 'ERROR_GENERAL_LIMIT_AMOUNT',
			'100.550.311' => 'ERROR_GENERAL_LIMIT_AMOUNT',

			'800.120.101' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',
			'800.120.100' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',
			'800.120.102' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',
			'800.120.103' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',
			'800.120.200' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',
			'800.120.201' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',
			'800.120.202' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',
			'800.120.203' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',

			'800.100.152' => 'ERROR_CC_DECLINED_AUTH',
			'000.400.106' => 'ERROR_CC_DECLINED_AUTH',
			'000.400.105' => 'ERROR_CC_DECLINED_AUTH',
			'000.400.103' => 'ERROR_CC_DECLINED_AUTH',

			'100.380.501' => 'ERROR_GENERAL_DECLINED_RISK',

			'800.400.151' => 'ERROR_CC_ADDRESS',
			'800.400.150' => 'ERROR_CC_ADDRESS',

			'100.400.300' => 'ERROR_GENERAL_CANCEL',
			'100.396.101' => 'ERROR_GENERAL_CANCEL',
			'900.300.600' => 'ERROR_GENERAL_CANCEL',

			'800.100.501' => 'ERROR_CC_RECURRING',
			'800.100.500' => 'ERROR_CC_RECURRING',

			'800.100.178' => 'ERROR_CC_REPEATED',
			'800.300.500' => 'ERROR_CC_REPEATED',
			'800.300.501' => 'ERROR_CC_REPEATED',

			'800.700.101' => 'ERROR_GENERAL_ADDRESS',
			'800.700.201' => 'ERROR_GENERAL_ADDRESS',
			'800.700.500' => 'ERROR_GENERAL_ADDRESS',
			'800.800.102' => 'ERROR_GENERAL_ADDRESS',
			'800.800.202' => 'ERROR_GENERAL_ADDRESS',
			'800.800.302' => 'ERROR_GENERAL_ADDRESS',
			'800.900.101' => 'ERROR_GENERAL_ADDRESS',
			'800.100.198' => 'ERROR_GENERAL_ADDRESS',
			'000.100.201' => 'ERROR_GENERAL_ADDRESS',

			'100.400.121' => 'ERROR_GENERAL_BLACKLIST',
			'800.100.172' => 'ERROR_GENERAL_BLACKLIST',
			'800.200.159' => 'ERROR_GENERAL_BLACKLIST',
			'800.200.160' => 'ERROR_GENERAL_BLACKLIST',
			'800.200.165' => 'ERROR_GENERAL_BLACKLIST',
			'800.200.202' => 'ERROR_GENERAL_BLACKLIST',
			'800.200.208' => 'ERROR_GENERAL_BLACKLIST',
			'800.200.220' => 'ERROR_GENERAL_BLACKLIST',
			'800.300.101' => 'ERROR_GENERAL_BLACKLIST',
			'800.300.102' => 'ERROR_GENERAL_BLACKLIST',
			'800.300.200' => 'ERROR_GENERAL_BLACKLIST',
			'800.300.301' => 'ERROR_GENERAL_BLACKLIST',
			'800.300.302' => 'ERROR_GENERAL_BLACKLIST',

			'000.100.200' => 'ERROR_GENERAL_GENERAL',
			'000.100.202' => 'ERROR_GENERAL_GENERAL',
			'000.100.206' => 'ERROR_GENERAL_GENERAL',
			'000.100.207' => 'ERROR_GENERAL_GENERAL',
			'000.100.208' => 'ERROR_GENERAL_GENERAL',
			'000.100.209' => 'ERROR_GENERAL_GENERAL',
			'000.100.210' => 'ERROR_GENERAL_GENERAL',
			'000.100.220' => 'ERROR_GENERAL_GENERAL',
			'000.100.221' => 'ERROR_GENERAL_GENERAL',
			'000.100.222' => 'ERROR_GENERAL_GENERAL',
			'000.100.223' => 'ERROR_GENERAL_GENERAL',
			'000.100.224' => 'ERROR_GENERAL_GENERAL',
			'000.100.225' => 'ERROR_GENERAL_GENERAL',
			'000.100.226' => 'ERROR_GENERAL_GENERAL',
			'000.100.227' => 'ERROR_GENERAL_GENERAL',
			'000.100.228' => 'ERROR_GENERAL_GENERAL',
			'000.100.229' => 'ERROR_GENERAL_GENERAL',
			'000.100.230' => 'ERROR_GENERAL_GENERAL',
			'000.100.299' => 'ERROR_GENERAL_GENERAL',
			'000.400.102' => 'ERROR_GENERAL_GENERAL',
			'000.400.200' => 'ERROR_GENERAL_GENERAL',
			'100.211.105' => 'ERROR_GENERAL_GENERAL',
			'100.211.106' => 'ERROR_GENERAL_GENERAL',
			'100.212.101' => 'ERROR_GENERAL_GENERAL',
			'100.212.102' => 'ERROR_GENERAL_GENERAL',
			'100.212.103' => 'ERROR_GENERAL_GENERAL',
			'100.250.100' => 'ERROR_GENERAL_GENERAL',
			'100.370.100' => 'ERROR_GENERAL_GENERAL',
			'100.380.100' => 'ERROR_GENERAL_GENERAL',
			'100.390.110' => 'ERROR_GENERAL_GENERAL',
			'100.390.113' => 'ERROR_GENERAL_GENERAL',
			'100.395.501' => 'ERROR_GENERAL_GENERAL',
			'100.396.102' => 'ERROR_GENERAL_GENERAL',
			'100.396.103' => 'ERROR_GENERAL_GENERAL',
			'100.396.104' => 'ERROR_GENERAL_GENERAL',
			'100.396.106' => 'ERROR_GENERAL_GENERAL',
			'100.396.201' => 'ERROR_GENERAL_GENERAL',
			'100.397.101' => 'ERROR_GENERAL_GENERAL',
			'100.397.102' => 'ERROR_GENERAL_GENERAL',
			'100.400.007' => 'ERROR_GENERAL_GENERAL',
			'100.400.041' => 'ERROR_GENERAL_GENERAL',
			'100.400.042' => 'ERROR_GENERAL_GENERAL',
			'100.400.043' => 'ERROR_GENERAL_GENERAL',
			'100.400.044' => 'ERROR_GENERAL_GENERAL',
			'100.400.045' => 'ERROR_GENERAL_GENERAL',
			'100.400.051' => 'ERROR_GENERAL_GENERAL',
			'100.400.061' => 'ERROR_GENERAL_GENERAL',
			'100.400.063' => 'ERROR_GENERAL_GENERAL',
			'100.400.064' => 'ERROR_GENERAL_GENERAL',
			'100.400.065' => 'ERROR_GENERAL_GENERAL',
			'100.400.071' => 'ERROR_GENERAL_GENERAL',
			'100.400.083' => 'ERROR_GENERAL_GENERAL',
			'100.400.084' => 'ERROR_GENERAL_GENERAL',
			'100.400.085' => 'ERROR_GENERAL_GENERAL',
			'100.400.091' => 'ERROR_GENERAL_GENERAL',
			'100.400.122' => 'ERROR_GENERAL_GENERAL',
			'100.400.130' => 'ERROR_GENERAL_GENERAL',
			'100.400.139' => 'ERROR_GENERAL_GENERAL',
			'100.400.140' => 'ERROR_GENERAL_GENERAL',
			'100.400.243' => 'ERROR_GENERAL_GENERAL',
			'100.400.301' => 'ERROR_GENERAL_GENERAL',
			'100.400.303' => 'ERROR_GENERAL_GENERAL',
			'100.400.304' => 'ERROR_GENERAL_GENERAL',
			'100.400.305' => 'ERROR_GENERAL_GENERAL',
			'100.400.306' => 'ERROR_GENERAL_GENERAL',
			'100.400.307' => 'ERROR_GENERAL_GENERAL',
			'100.400.308' => 'ERROR_GENERAL_GENERAL',
			'100.400.309' => 'ERROR_GENERAL_GENERAL',
			'100.400.310' => 'ERROR_GENERAL_GENERAL',
			'100.400.311' => 'ERROR_GENERAL_GENERAL',
			'100.400.312' => 'ERROR_GENERAL_GENERAL',
			'100.400.313' => 'ERROR_GENERAL_GENERAL',
			'100.400.314' => 'ERROR_GENERAL_GENERAL',
			'100.400.315' => 'ERROR_GENERAL_GENERAL',
			'100.400.318' => 'ERROR_GENERAL_GENERAL',
			'100.400.320' => 'ERROR_GENERAL_GENERAL',
			'100.400.321' => 'ERROR_GENERAL_GENERAL',
			'100.400.322' => 'ERROR_GENERAL_GENERAL',
			'100.400.323' => 'ERROR_GENERAL_GENERAL',
			'100.400.324' => 'ERROR_GENERAL_GENERAL',
			'100.400.325' => 'ERROR_GENERAL_GENERAL',
			'100.400.326' => 'ERROR_GENERAL_GENERAL',
			'100.400.327' => 'ERROR_GENERAL_GENERAL',
			'100.400.328' => 'ERROR_GENERAL_GENERAL',
			'100.400.500' => 'ERROR_GENERAL_GENERAL',
			'100.500.101' => 'ERROR_GENERAL_GENERAL',
			'100.500.201' => 'ERROR_GENERAL_GENERAL',
			'100.500.301' => 'ERROR_GENERAL_GENERAL',
			'100.500.302' => 'ERROR_GENERAL_GENERAL',
			'100.550.300' => 'ERROR_GENERAL_GENERAL',
			'100.550.301' => 'ERROR_GENERAL_GENERAL',
			'100.550.303' => 'ERROR_GENERAL_GENERAL',
			'100.550.312' => 'ERROR_GENERAL_GENERAL',
			'100.550.400' => 'ERROR_GENERAL_GENERAL',
			'100.550.401' => 'ERROR_GENERAL_GENERAL',
			'100.550.601' => 'ERROR_GENERAL_GENERAL',
			'100.550.603' => 'ERROR_GENERAL_GENERAL',
			'100.550.605' => 'ERROR_GENERAL_GENERAL',
			'100.600.500' => 'ERROR_GENERAL_GENERAL',
			'100.700.100' => 'ERROR_GENERAL_GENERAL',
			'100.700.101' => 'ERROR_GENERAL_GENERAL',
			'100.700.200' => 'ERROR_GENERAL_GENERAL',
			'100.700.201' => 'ERROR_GENERAL_GENERAL',
			'100.700.300' => 'ERROR_GENERAL_GENERAL',
			'100.700.400' => 'ERROR_GENERAL_GENERAL',
			'100.700.500' => 'ERROR_GENERAL_GENERAL',
			'100.700.800' => 'ERROR_GENERAL_GENERAL',
			'100.700.801' => 'ERROR_GENERAL_GENERAL',
			'100.700.802' => 'ERROR_GENERAL_GENERAL',
			'100.700.810' => 'ERROR_GENERAL_GENERAL',
			'100.800.100' => 'ERROR_GENERAL_GENERAL',
			'100.800.101' => 'ERROR_GENERAL_GENERAL',
			'100.800.102' => 'ERROR_GENERAL_GENERAL',
			'100.800.200' => 'ERROR_GENERAL_GENERAL',
			'100.800.201' => 'ERROR_GENERAL_GENERAL',
			'100.800.202' => 'ERROR_GENERAL_GENERAL',
			'100.800.300' => 'ERROR_GENERAL_GENERAL',
			'100.800.301' => 'ERROR_GENERAL_GENERAL',
			'100.800.302' => 'ERROR_GENERAL_GENERAL',
			'100.800.400' => 'ERROR_GENERAL_GENERAL',
			'100.800.401' => 'ERROR_GENERAL_GENERAL',
			'100.800.500' => 'ERROR_GENERAL_GENERAL',
			'100.800.501' => 'ERROR_GENERAL_GENERAL',
			'100.900.100' => 'ERROR_GENERAL_GENERAL',
			'100.900.101' => 'ERROR_GENERAL_GENERAL',
			'100.900.105' => 'ERROR_GENERAL_GENERAL',
			'100.900.200' => 'ERROR_GENERAL_GENERAL',
			'100.900.300' => 'ERROR_GENERAL_GENERAL',
			'100.900.301' => 'ERROR_GENERAL_GENERAL',
			'100.900.400' => 'ERROR_GENERAL_GENERAL',
			'100.900.401' => 'ERROR_GENERAL_GENERAL',
			'100.900.450' => 'ERROR_GENERAL_GENERAL',
			'100.900.500' => 'ERROR_GENERAL_GENERAL',
			'200.100.101' => 'ERROR_GENERAL_GENERAL',
			'200.100.102' => 'ERROR_GENERAL_GENERAL',
			'200.100.103' => 'ERROR_GENERAL_GENERAL',
			'200.100.150' => 'ERROR_GENERAL_GENERAL',
			'200.100.151' => 'ERROR_GENERAL_GENERAL',
			'200.100.199' => 'ERROR_GENERAL_GENERAL',
			'200.100.201' => 'ERROR_GENERAL_GENERAL',
			'200.100.300' => 'ERROR_GENERAL_GENERAL',
			'200.100.301' => 'ERROR_GENERAL_GENERAL',
			'200.100.302' => 'ERROR_GENERAL_GENERAL',
			'200.100.401' => 'ERROR_GENERAL_GENERAL',
			'200.100.402' => 'ERROR_GENERAL_GENERAL',
			'200.100.403' => 'ERROR_GENERAL_GENERAL',
			'200.100.404' => 'ERROR_GENERAL_GENERAL',
			'200.100.501' => 'ERROR_GENERAL_GENERAL',
			'200.100.502' => 'ERROR_GENERAL_GENERAL',
			'200.100.503' => 'ERROR_GENERAL_GENERAL',
			'200.100.504' => 'ERROR_GENERAL_GENERAL',
			'200.200.106' => 'ERROR_GENERAL_GENERAL',
			'200.300.403' => 'ERROR_GENERAL_GENERAL',
			'200.300.404' => 'ERROR_GENERAL_GENERAL',
			'200.300.405' => 'ERROR_GENERAL_GENERAL',
			'200.300.406' => 'ERROR_GENERAL_GENERAL',
			'200.300.407' => 'ERROR_GENERAL_GENERAL',
			'500.100.201' => 'ERROR_GENERAL_GENERAL',
			'500.100.202' => 'ERROR_GENERAL_GENERAL',
			'500.100.203' => 'ERROR_GENERAL_GENERAL',
			'500.100.301' => 'ERROR_GENERAL_GENERAL',
			'500.100.302' => 'ERROR_GENERAL_GENERAL',
			'500.100.303' => 'ERROR_GENERAL_GENERAL',
			'500.100.304' => 'ERROR_GENERAL_GENERAL',
			'500.100.401' => 'ERROR_GENERAL_GENERAL',
			'500.100.402' => 'ERROR_GENERAL_GENERAL',
			'500.100.403' => 'ERROR_GENERAL_GENERAL',
			'500.200.101' => 'ERROR_GENERAL_GENERAL',
			'600.100.100' => 'ERROR_GENERAL_GENERAL',
			'600.200.100' => 'ERROR_GENERAL_GENERAL',
			'600.200.200' => 'ERROR_GENERAL_GENERAL',
			'600.200.201' => 'ERROR_GENERAL_GENERAL',
			'600.200.202' => 'ERROR_GENERAL_GENERAL',
			'600.200.300' => 'ERROR_GENERAL_GENERAL',
			'600.200.310' => 'ERROR_GENERAL_GENERAL',
			'600.200.400' => 'ERROR_GENERAL_GENERAL',
			'600.200.500' => 'ERROR_GENERAL_GENERAL',
			'600.200.600' => 'ERROR_GENERAL_GENERAL',
			'600.200.700' => 'ERROR_GENERAL_GENERAL',
			'600.200.800' => 'ERROR_GENERAL_GENERAL',
			'600.200.810' => 'ERROR_GENERAL_GENERAL',
			'700.100.100' => 'ERROR_GENERAL_GENERAL',
			'700.100.200' => 'ERROR_GENERAL_GENERAL',
			'700.100.300' => 'ERROR_GENERAL_GENERAL',
			'700.100.400' => 'ERROR_GENERAL_GENERAL',
			'700.100.500' => 'ERROR_GENERAL_GENERAL',
			'700.100.600' => 'ERROR_GENERAL_GENERAL',
			'700.100.700' => 'ERROR_GENERAL_GENERAL',
			'700.100.701' => 'ERROR_GENERAL_GENERAL',
			'700.100.710' => 'ERROR_GENERAL_GENERAL',
			'700.300.500' => 'ERROR_GENERAL_GENERAL',
			'700.400.000' => 'ERROR_GENERAL_GENERAL',
			'700.400.400' => 'ERROR_GENERAL_GENERAL',
			'700.400.402' => 'ERROR_GENERAL_GENERAL',
			'700.400.410' => 'ERROR_GENERAL_GENERAL',
			'700.400.420' => 'ERROR_GENERAL_GENERAL',
			'700.400.562' => 'ERROR_GENERAL_GENERAL',
			'700.400.570' => 'ERROR_GENERAL_GENERAL',
			'700.400.700' => 'ERROR_GENERAL_GENERAL',
			'700.450.001' => 'ERROR_GENERAL_GENERAL',
			'800.100.100' => 'ERROR_GENERAL_GENERAL',
			'800.100.150' => 'ERROR_GENERAL_GENERAL',
			'800.100.179' => 'ERROR_GENERAL_GENERAL',
			'800.110.100' => 'ERROR_GENERAL_GENERAL',
			'800.120.300' => 'ERROR_GENERAL_GENERAL',
			'800.120.401' => 'ERROR_GENERAL_GENERAL',
			'800.121.100' => 'ERROR_GENERAL_GENERAL',
			'800.130.100' => 'ERROR_GENERAL_GENERAL',
			'800.140.100' => 'ERROR_GENERAL_GENERAL',
			'800.140.101' => 'ERROR_GENERAL_GENERAL',
			'800.140.110' => 'ERROR_GENERAL_GENERAL',
			'800.140.111' => 'ERROR_GENERAL_GENERAL',
			'800.140.112' => 'ERROR_GENERAL_GENERAL',
			'800.140.113' => 'ERROR_GENERAL_GENERAL',
			'800.160.100' => 'ERROR_GENERAL_GENERAL',
			'800.160.110' => 'ERROR_GENERAL_GENERAL',
			'800.160.120' => 'ERROR_GENERAL_GENERAL',
			'800.160.130' => 'ERROR_GENERAL_GENERAL',
			'800.400.100' => 'ERROR_GENERAL_GENERAL',
			'800.400.101' => 'ERROR_GENERAL_GENERAL',
			'800.400.102' => 'ERROR_GENERAL_GENERAL',
			'800.400.103' => 'ERROR_GENERAL_GENERAL',
			'800.400.104' => 'ERROR_GENERAL_GENERAL',
			'800.400.105' => 'ERROR_GENERAL_GENERAL',
			'800.400.110' => 'ERROR_GENERAL_GENERAL',
			'800.400.500' => 'ERROR_GENERAL_GENERAL',
			'800.500.100' => 'ERROR_GENERAL_GENERAL',
			'800.500.110' => 'ERROR_GENERAL_GENERAL',
			'800.600.100' => 'ERROR_GENERAL_GENERAL',
			'800.700.100' => 'ERROR_GENERAL_GENERAL',
			'800.800.800' => 'ERROR_GENERAL_GENERAL',
			'800.800.801' => 'ERROR_GENERAL_GENERAL',
			'800.900.100' => 'ERROR_GENERAL_GENERAL',
			'800.900.201' => 'ERROR_GENERAL_GENERAL',
			'800.900.303' => 'ERROR_GENERAL_GENERAL',
			'800.900.401' => 'ERROR_GENERAL_GENERAL',
			'900.100.100' => 'ERROR_GENERAL_GENERAL',
			'900.100.200' => 'ERROR_GENERAL_GENERAL',
			'900.100.201' => 'ERROR_GENERAL_GENERAL',
			'900.100.202' => 'ERROR_GENERAL_GENERAL',
			'900.100.203' => 'ERROR_GENERAL_GENERAL',
			'900.100.300' => 'ERROR_GENERAL_GENERAL',
			'900.100.400' => 'ERROR_GENERAL_GENERAL',
			'900.100.500' => 'ERROR_GENERAL_GENERAL',
			'900.100.600' => 'ERROR_GENERAL_GENERAL',
			'900.200.100' => 'ERROR_GENERAL_GENERAL',
			'900.400.100' => 'ERROR_GENERAL_GENERAL',
			'999.999.999' => 'ERROR_GENERAL_GENERAL',

			'000.400.107' => 'ERROR_GENERAL_TIMEOUT',
			'100.395.502' => 'ERROR_GENERAL_TIMEOUT',

			'100.395.101' => 'ERROR_GIRO_NOSUPPORT',
			'100.395.102' => 'ERROR_GIRO_NOSUPPORT',

			'700.400.100' => 'ERROR_CAPTURE_BACKEND',
			'700.400.101' => 'ERROR_CAPTURE_BACKEND',
			'700.400.510' => 'ERROR_CAPTURE_BACKEND',

			'800.100.500' => 'ERROR_REORDER_BACKEND',
			'800.100.501' => 'ERROR_REORDER_BACKEND',

			'700.300.300' => 'ERROR_REFUND_BACKEND',
			'700.300.400' => 'ERROR_REFUND_BACKEND',
			'700.300.600' => 'ERROR_REFUND_BACKEND',
			'700.300.700' => 'ERROR_REFUND_BACKEND',
			'700.400.200' => 'ERROR_REFUND_BACKEND',
			'700.400.300' => 'ERROR_REFUND_BACKEND',
			'700.400.520' => 'ERROR_REFUND_BACKEND',
			'700.400.530' => 'ERROR_REFUND_BACKEND',
			'700.300.100' => 'ERROR_REFUND_BACKEND',

			'700.400.560' => 'ERROR_RECEIPT_BACKEND',
			'700.400.561' => 'ERROR_RECEIPT_BACKEND',

			'800.900.200' => 'ERROR_ADDRESS_PHONE',
		);
		if ( $code ) {
			return array_key_exists( $code, $error_messages ) ? $error_messages[ $code ] : 'ERROR_UNKNOWN';
		} else {
			return 'ERROR_UNKNOWN';
		}
	}

	/**
	 * Get error identifier for back office operations
	 *
	 * @param string $code code from respond.
	 * @return string
	 */
	public static function get_error_identifier_backend( $code ) {
		$error_messages = array(
			'700.400.100' => 'ERROR_CAPTURE_BACKEND',
			'700.400.101' => 'ERROR_CAPTURE_BACKEND',
			'700.400.510' => 'ERROR_CAPTURE_BACKEND',

			'800.100.500' => 'ERROR_REORDER_BACKEND',
			'800.100.501' => 'ERROR_REORDER_BACKEND',

			'700.300.300' => 'ERROR_REFUND_BACKEND',
			'700.300.400' => 'ERROR_REFUND_BACKEND',
			'700.300.600' => 'ERROR_REFUND_BACKEND',
			'700.300.700' => 'ERROR_REFUND_BACKEND',
			'700.400.200' => 'ERROR_REFUND_BACKEND',
			'700.400.300' => 'ERROR_REFUND_BACKEND',
			'700.400.520' => 'ERROR_REFUND_BACKEND',
			'700.400.530' => 'ERROR_REFUND_BACKEND',
			'700.300.100' => 'ERROR_REFUND_BACKEND',

			'700.400.560' => 'ERROR_RECEIPT_BACKEND',
			'700.400.561' => 'ERROR_RECEIPT_BACKEND',
		);
		if ( $code ) {
			return array_key_exists( $code, $error_messages ) ? $error_messages[ $code ] : 'ERROR_GENERAL_PROCESSING';
		} else {
			return 'ERROR_GENERAL_PROCESSING';
		}
	}

	/**
	 * Check if response code from gateway is 'in review'
	 *
	 * @param string $code code from respond.
	 * @return boolean
	 */
	public static function is_success_review( $code ) {
		$in_reviews = array(
			'000.400.000',
			'000.400.010',
			'000.400.020',
			'000.400.030',
			'000.400.040',
			'000.400.050',
			'000.400.060',
			'000.400.070',
			'000.400.080',
			'000.400.090',
		);
		if ( in_array( $code, $in_reviews, true ) ) {
			return true;
		} else {
			return false;
		}
	}

}
