<?php
/**
 * Generic Shop Plugin Installation Process
 *
 * The class is used for General Setting Tabs
 * Copyright (c) Generic Shop
 *
 * @class      Genericshop_General_Settings
 * @package  Genericshop/Classes
 * @located at  /includes/admin/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * The class is used for General Setting Tabs
 */
class Genericshop_General_Settings {

	/**
	 * Tab name
	 *
	 * @var string
	 */
	public static $tab_name      = 'genericshop_settings';
	/**
	 * Mandatory field
	 *
	 * @var array
	 */
	public static $mandatory_field  = array(
		'genericshop_general_merchant_email',
		'genericshop_general_merchant_no',
		'genericshop_general_shop_url',
		'genericshop_general_merchant_location',
	);

	/**
	 * Main function of the class
	 */
	public static function init() {
		$page_request = Genericshop_General_Functions::grs_get_request_value( 'page' );
		$tab_request = Genericshop_General_Functions::grs_get_request_value( 'tab' );
		if ( 'wc-settings' === $page_request && 'genericshop_settings' === $tab_request ) { // safe request.
			self::grs_save_tab_settings();
		}

		add_filter( 'woocommerce_settings_tabs_array',array( __CLASS__, 'add_settings_tab' ), 50 );
		add_action( 'woocommerce_settings_tabs_genericshop_settings', array( __CLASS__, 'add_settings_page' ) );
		add_action( 'woocommerce_update_options_genericshop_settings', array( __CLASS__, 'update_settings' ) );
	}

	/**
	 * Add Generic Shop tab to WooCommerce
	 * Calls from the hook "woocommerce_settings_tabs_array"
	 *
	 * @param array $woocommerce_tab WooCommerce tab.
	 * @return array $woocommerce_tab
	 */
	public static function add_settings_tab( $woocommerce_tab ) {
		$woocommerce_tab[ self::$tab_name ] = 'Generic Shop ' . __( 'BACKEND_CH_GENERAL', 'wc-genericshop' );
		return $woocommerce_tab;
	}

	/**
	 * Add setting fields to the general setting page
	 * Calls from the hook "woocommerce_settings_tabs_{tab_name}"
	 */
	public static function add_settings_page() {
		woocommerce_admin_fields( self::grs_settings_fields() );
	}

	/**
	 * Update setting fields to the general setting page
	 * Calls from the hook "woocommerce_update_options_{tab_name}"
	 */
	public static function update_settings() {
		woocommerce_update_options( self::grs_settings_fields() );
	}

	/**
	 * Add setting fields for Generic Shop general setting
	 */
	public static function grs_settings_fields() {
		global $genericshop_payments;

		$settings = apply_filters( 'woocommerce_' . self::$tab_name, array(
			array(
				'title' => 'Generic Shop ' . __( 'BACKEND_CH_GENERAL', 'wc-genericshop' ),
				'id'    => 'genericshop_general_settings',
				'desc'  => '',
				'type'  => 'title',
			),
			array(
				'title' => __( 'BACKEND_CH_LOGIN', 'wc-genericshop' ),
				'id'    => 'genericshop_general_login',
				'css'   => 'width:25em;',
				'type'  => 'text',
			),
			array(
				'title' => __( 'BACKEND_CH_PASSWORD', 'wc-genericshop' ),
				'id'    => 'genericshop_general_password',
				'css'   => 'width:25em;',
				'type'  => 'text',
			),
			array(
				'title' => __( 'BACKEND_CH_RECURRING', 'wc-genericshop' ),
				'id'    => 'genericshop_general_recurring',
				'css'   => 'width:25em; padding: 1px;',
				'type'  => 'select',
				'options' => array(
					'0' => __( 'BACKEND_BT_NO', 'wc-genericshop' ),
					'1' => __( 'BACKEND_BT_YES', 'wc-genericshop' ),
				),
				'default' => '1',
			),
			array(
				'title' => __( 'BACKEND_GENERAL_MERCHANTEMAIL', 'wc-genericshop' ) . ' * ',
				'id'    => 'genericshop_general_merchant_email',
				'css'   => 'width:25em;',
				'type'  => 'text',
			),
			array(
				'title' => __( 'GENERICSHOP_BACKEND_GENERAL_MERCHANTNO', 'wc-genericshop' ) . ' * ',
				'id'    => 'genericshop_general_merchant_no',
				'css'   => 'width:25em;',
				'type'  => 'text',
				'desc'  => '<br />' . __( 'BACKEND_TT_MERCHANT_ID', 'wc-genericshop' ),
			),
			array(
				'title' => __( 'BACKEND_GENERAL_SHOPURL', 'wc-genericshop' ) . ' * ',
				'id'    => 'genericshop_general_shop_url',
				'css'   => 'width:25em;',
				'type'  => 'text',
			),
			array(
				'title' => __( 'BACKEND_CH_DOB_GENDER', 'wc-genericshop' ),
				'id'    => 'genericshop_general_dob_gender',
				'css'   => 'width:25em; padding: 1px;',
				'type'  => 'select',
				'desc'  => '<br />' . __( 'BACKEND_TT_DOB_GENDER', 'wc-genericshop' ),
				'options' => array(
					'0' => __( 'BACKEND_BT_NO', 'wc-genericshop' ),
					'1' => __( 'BACKEND_BT_YES', 'wc-genericshop' ),
				),
				'default' => '0',
			),
			array(
				'title' => __( 'BACKEND_GENERAL_MERCHANT_LOCATION_TITLE', 'wc-genericshop' ) . ' * ',
				'id'    => 'genericshop_general_merchant_location',
				'css'   => 'width:25em;',
				'type'  => 'text',
				'desc'  => '<br />' . __( 'BACKEND_GENERAL_MERCHANT_LOCATION_DESC', 'wc-genericshop' ),
			),
			array(
				'type' => 'sectionend',
				'id' => 'genericshop_vendor_script',
			),
		) );
		return apply_filters( 'woocommerce_' . self::$tab_name, $settings );
	}

	/**
	 * Redirect and show error message if empty mandatory fields when save the general settings
	 */
	public static function grs_save_tab_settings() {
		$save_request = Genericshop_General_Functions::grs_get_request_value( 'save' );
		if ( $save_request ) {
			$is_fill_mandatory_fields = self::grs_is_fill_mandatory_fields( $_REQUEST ); // input var okay.

			if ( ! $is_fill_mandatory_fields ) {
				$get = isset( $_GET ) ? $_GET : null; // input var okay.
				$redirect = get_admin_url() . 'admin.php?' . http_build_query( $get, '', '&' );
				$redirect = remove_query_arg( 'save' );
				$error_message = __( 'ERROR_GENERAL_MANDATORY', 'wc-genericshop' );
				$redirect = add_query_arg( 'wc_error', rawurlencode( esc_attr( $error_message ) ), $redirect );
				wp_safe_redirect( $redirect );
				exit();
			} else {
				$redirect = get_admin_url() . 'admin.php?' . http_build_query( $_GET, '', '&' ); // input var okay.
				$redirect = remove_query_arg( 'wc_error' );
				$message = __( 'Your settings have been saved.', 'woocommerce' );
				$redirect = add_query_arg( 'wc_message', rawurlencode( esc_attr( $message ) ), $redirect );
				wp_safe_redirect( $redirect );
			}
		}
	}

	/**
	 * Validate mandatory fields from the general settings
	 *
	 * @param array $request from $_REQUEST.
	 * @return mixed
	 */
	public static function grs_is_fill_mandatory_fields( $request ) {

		foreach ( $request as $fields_name => $field_value ) {
			if ( in_array( $fields_name, self::$mandatory_field, true ) ) {
				if ( trim( $field_value ) === '' ) {
					return false;
				}
			}
		}

		return true;
	}
}

Genericshop_General_Settings::init();
